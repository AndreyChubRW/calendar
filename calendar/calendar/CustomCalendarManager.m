//
//  CustomCalendarManager.m
//  calendar
//
//  Created by Andrey on 9/22/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "CustomCalendarManager.h"

@implementation CustomCalendarManager

static CustomCalendarManager *sharedInstance = nil;

+ (instancetype)sharedInstance {
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CustomCalendarManager alloc] init];
    });
    return sharedInstance;
}

- (void)createColuorCalendars {
    
    if ([self calendarsIsExisting]) {
        return;
        
    } else {
        
        NSArray *calendarTitles = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J"];
        NSArray *colors = [self getColoursPallet];
        
        for (int i = 0; i < calendarTitles.count; i++) {
            
            EKCalendar *calendar1 = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:self.eventKitSupport.eventStore];
            
            EKSource *theSource = nil;
            for (EKSource *source in self.eventKitSupport.eventStore.sources) {
                if (source.sourceType == EKSourceTypeLocal) {
                    theSource = source;
                    break;
                }
            }
            
            if (theSource) {
                
                calendar1.source = theSource;
                
            } else {
                
                NSLog(@"Error: Local source not available");
            }
            
            UIColor *colour = colors[i];
            
            calendar1.CGColor = colour.CGColor;
            calendar1.title = calendarTitles[i];
            
            NSError *error = nil;
            BOOL result = [self.eventKitSupport.eventStore saveCalendar:calendar1 commit:YES error:&error];
            if (result) {
                NSLog(@"Saved calendar to event store. With Identifier %@", calendar1.calendarIdentifier);
            } else {
                NSLog(@"Error saving calendar: %@.", error);
            }
        }
    }
}

- (EKCalendar *)getCalendarAtIndex:(NSUInteger)index {
    
    NSArray *allCalendars = [self.eventKitSupport.eventStore calendarsForEntityType:EKEntityTypeEvent];
    NSMutableArray *customCalendars = [[NSMutableArray alloc] init];
    
    for (EKCalendar *cal in allCalendars) {
        if ([cal.title isEqualToString:@"A"] || [cal.title isEqualToString:@"B"] || [cal.title isEqualToString:@"C"] || [cal.title isEqualToString:@"D"] || [cal.title isEqualToString:@"E"] || [cal.title isEqualToString:@"F"] || [cal.title isEqualToString:@"G"] || [cal.title isEqualToString:@"H"] || [cal.title isEqualToString:@"I"] || [cal.title isEqualToString:@"J"]) {
            
            [customCalendars addObject:cal];
        }
        else {
            NSLog(@"");
        }
    }
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES selector:@selector(compare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [customCalendars sortedArrayUsingDescriptors:sortDescriptors];
    
    return [sortedArray objectAtIndex:index];
}

- (void)deleteCustomCalendars {
    
    NSArray *calendarsArray = [self.eventKitSupport.eventStore calendarsForEntityType:EKEntityTypeEvent];
    
    for (EKCalendar *cal in calendarsArray) {
        if ([cal.title isEqualToString:@"A"] || [cal.title isEqualToString:@"B"] || [cal.title isEqualToString:@"C"] || [cal.title isEqualToString:@"D"] || [cal.title isEqualToString:@"E"] || [cal.title isEqualToString:@"F"] || [cal.title isEqualToString:@"G"] || [cal.title isEqualToString:@"H"] || [cal.title isEqualToString:@"I"] || [cal.title isEqualToString:@"J"]) {
            [self.eventKitSupport.eventStore removeCalendar:cal commit:YES error:nil];
            NSLog(@"");
        }
        else {
            NSLog(@"");
        }
    }
}


- (NSArray *)getColoursPallet {
    
    UIColor *borderColor1 = [UIColor colorWithRed:(CGFloat)(223.f/255.f) green:(CGFloat)(157.f/255.f) blue:(CGFloat)(168.f/255.f) alpha:1];
    UIColor *borderColor2 = [UIColor colorWithRed:(CGFloat)(202.f/255.f) green:(CGFloat)(127.f/255.f) blue:(CGFloat)(76.f/255.f) alpha:1];
    UIColor *borderColor3 = [UIColor colorWithRed:(CGFloat)(186.f/255.f) green:(CGFloat)(168.f/255.f) blue:(CGFloat)(42.f/255.f) alpha:1];
    UIColor *borderColor4 = [UIColor colorWithRed:(CGFloat)(117.f/255.f) green:(CGFloat)(192.f/255.f) blue:(CGFloat)(65.f/255.f) alpha:1];
    UIColor *borderColor5 = [UIColor colorWithRed:(CGFloat)(131.f/255.f) green:(CGFloat)(158.f/255.f) blue:(CGFloat)(216.f/255.f) alpha:1];
    UIColor *borderColor6 = [UIColor colorWithRed:(CGFloat)(71.f/255.f) green:(CGFloat)(170.f/255.f) blue:(CGFloat)(192.f/255.f) alpha:1];
    UIColor *borderColor7 = [UIColor colorWithRed:(CGFloat)(156.f/255.f) green:(CGFloat)(182.f/255.f) blue:(CGFloat)(49.f/255.f) alpha:1];
    UIColor *borderColor8 = [UIColor colorWithRed:(CGFloat)(55.f/255.f) green:(CGFloat)(193.f/255.f) blue:(CGFloat)(166.f/255.f) alpha:1];
    UIColor *borderColor9 = [UIColor colorWithRed:(CGFloat)(217.f/255.f) green:(CGFloat)(124.f/255.f) blue:(CGFloat)(105.f/255.f) alpha:1];
    UIColor *borderColor10 = [UIColor colorWithRed:(CGFloat)(216.f/255.f) green:(CGFloat)(113.f/255.f) blue:(CGFloat)(203.f/255.f) alpha:1];
    
    NSArray *colorArray = @[borderColor1, borderColor2, borderColor3, borderColor4, borderColor5, borderColor6, borderColor7, borderColor8, borderColor9, borderColor10];
    
    return colorArray;
}

- (BOOL)calendarsIsExisting {
    
    NSArray *calendarsArray = [self.eventKitSupport.eventStore calendarsForEntityType:EKEntityTypeEvent];
    
    NSInteger iterator = 0;
    
    for (EKCalendar *cal in calendarsArray) {
        if ([cal.title isEqualToString:@"A"] || [cal.title isEqualToString:@"B"] || [cal.title isEqualToString:@"C"] || [cal.title isEqualToString:@"D"] || [cal.title isEqualToString:@"E"] || [cal.title isEqualToString:@"F"] || [cal.title isEqualToString:@"G"] || [cal.title isEqualToString:@"H"] || [cal.title isEqualToString:@"I"] || [cal.title isEqualToString:@"J"]) {
            
            iterator ++;
        }
        else {
        }
    }
    
    if (iterator  == 10) {
        
        return YES;
    }
    else {
        return NO;
    }
    
    
    return NO;
}

@end
