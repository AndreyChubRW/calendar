//
//  CustomRepeatTableViewCell.h
//  calendar
//
//  Created by Andrey on 9/21/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepeatCellModel.h"

@interface CustomRepeatTableViewCell : UITableViewCell

- (void)setCellDataWithDict:(RepeatCellModel *)dict;

@end
