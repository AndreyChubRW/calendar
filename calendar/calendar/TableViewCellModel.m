//
//  TableViewCellModel.m
//  calendar
//
//  Created by Andrey on 9/17/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "TableViewCellModel.h"

@interface TableViewCellModel ()

@end

@implementation TableViewCellModel

- (instancetype)initWithImageName:(NSString *)pictureName placeHolder:(NSString *)placeHolder cellText:(NSString *)cellText identifier:(NSString *)identifier height:(NSNumber *)height {
    
    self = [super init];
    
    if (self) {
        
        _pictureName = pictureName;
        _placeHolder = placeHolder;
        _cellText = cellText;
        _identifier = identifier;
        _height = height;
    }
    
    return self;
}

@end
