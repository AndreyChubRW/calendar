//
//  MonthViewController.swift
//  calendar
//
//  Created by forever on 8/23/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

import UIKit
//import CalendarLib


class MonthViewController:  MGCMonthPlannerEKViewController, CalendarViewControllerNavigation
{
    var delegate:CalendarViewControllerDelegate?
    var centerDate :NSDate?
        {
        get{
            let visibleRange = self.monthPlannerView.visibleDays
            if visibleRange != nil
            {
                let dayCount = self.calendar!.components(.Day, fromDate:visibleRange!.start, toDate:visibleRange!.end, options: []).day
                let comp = NSDateComponents()
                comp.day = dayCount / 2;
                let centerDate = self.calendar!.dateByAddingComponents(comp, toDate:visibleRange!.start, options:[])
                
                let startOfWeekDateComponents = calendar!.components([.YearForWeekOfYear, .WeekOfYear ], fromDate: centerDate!)
                let startOfWeek = self.calendar!.dateFromComponents(startOfWeekDateComponents)
                return startOfWeek
            }
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func monthPlannerViewDidScroll(view: MGCMonthPlannerView!) {
        
        super.monthPlannerViewDidScroll(view)
        let date: NSDate? = self.monthPlannerView.dayAtPoint(self.monthPlannerView.center)
        if (date != nil && (self as? CalendarViewController) != nil)
        {
            self.delegate!.calendarViewController(self, didShowDate: date!)
        }
    }
    
    override func monthPlannerView(view: MGCMonthPlannerView!, attributedStringForDayHeaderAtDate date: NSDate!) -> NSAttributedString!
    {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "d";
        let dayStr = dateFormatter.stringFromDate(date)
        
        let font = UIFont.systemFontOfSize(15)
        let attrStr = NSMutableAttributedString(string: dayStr, attributes: [ NSFontAttributeName: font ])
        
        let now = NSDate()
        if self.calendar!.mgc_isDate(date, sameDayAsDate: now)
        {
            let boldFont = UIFont.boldSystemFontOfSize(15)
            
            let mark = MGCCircleMark()
            mark.yOffset = boldFont.descender - mark.margin;
            
            //let theRangeText = str.rangeOfString(dayStr)
            let start = dayStr.startIndex.distanceTo(dayStr.startIndex)
            let length = dayStr.startIndex.distanceTo( dayStr.endIndex)
            let range = NSMakeRange(start, length)
            attrStr.addAttributes([NSFontAttributeName: boldFont, NSForegroundColorAttributeName: UIColor.whiteColor(), MGCCircleMarkAttributeName: mark], range: range)
            
            attrStr.processCircleMarksInRange(NSMakeRange(0, attrStr.length))
        }
        
        let para = NSMutableParagraphStyle()
        para.alignment = NSTextAlignment.Right;
        para.tailIndent = -6;
        
        attrStr.addAttributes([NSParagraphStyleAttributeName: para], range: NSMakeRange(0, attrStr.length))
        return attrStr
    }
    
    func moveToNextPageAnimated(animated: Bool) {
        let date = self.calendar!.mgc_nextStartOfMonthForDate(self.monthPlannerView.visibleDays.start)
        self.moveToDate(date, animated:animated)
    }
    
    func moveToPreviousPageAnimated(animated: Bool) {
        var date = self.calendar!.mgc_startOfMonthForDate(self.monthPlannerView.visibleDays.start)
        if self.monthPlannerView.visibleDays.start.isEqualToDate(date)  {
            let comps = NSDateComponents()
            comps.month = -1;
            date = self.calendar!.dateByAddingComponents(comps, toDate:date, options:[])
        }
        self.moveToDate(date, animated:animated)
    }
    
    func moveToDate(date: NSDate, animated: Bool) {
        
        if (self.monthPlannerView!.dateRange == nil || self.monthPlannerView!.dateRange.containsDate(date))
        {
            self.monthPlannerView!.scrollToDate(date, alignment:MGCMonthPlannerScrollAlignment.WeekRow ,animated:animated)
        }
    }
    
    
    //MARK: MGCMonthPlannerEKViewController
    
}