//
//  ViewController.swift
//  calendar
//
//  Created by forever on 8/17/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

import UIKit

class LeftViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        if let sideVC = (UIApplication.sharedApplication().delegate as! AppDelegate).sideVC {
            sideVC.collapsePanel()
        }
    }
    
    //UITableViewDelegate, UITableViewDataSource Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LeftViewTableViewCell") as! LeftViewTableViewCell
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
            selectedCell.contentView.backgroundColor = AppSetting.colorize(AppSetting.BG_COLOR)
            
            NSNotificationCenter.defaultCenter().postNotificationName("ShowSettingScreen", object: nil)
        }
    }
}

