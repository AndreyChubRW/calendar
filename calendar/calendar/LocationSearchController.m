//
//  LocationSearchController.m
//  calendar
//
//  Created by Andrey on 9/21/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "LocationSearchController.h"
#import "LocationTableViewCell.h"

@interface LocationSearchController () <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *userLocationBtn;

@property (strong, nonatomic) CLGeocoder *geocoder;
@property (strong, nonatomic) NSArray *tableData;
@property (strong, nonatomic) TableViewCellModel *cellModel;
@property (strong, nonatomic) EKEvent *event;

@end

@implementation LocationSearchController

- (instancetype)initLocationSearchControllerWithModel:(TableViewCellModel *)cellModel event:(EKEvent *)event {
    
    self = [[NSBundle mainBundle] loadNibNamed:@"LocationSearchController" owner:self options:nil][0];
    
    if (self) {
        
        [LocationManager startTrackLocation];
        self.cellModel = cellModel;
        if ([self.cellModel.cellText isEqualToString:@""]) {
            self.searchBar.text = @"";
        }
        else {
            self.searchBar.text = cellModel.cellText;
        }
        self.geocoder = [[CLGeocoder alloc] init];
        self.event = event;
    }
    return self;
}

-(void)viewDidLoad {
    [self.searchBar becomeFirstResponder];
}

- (IBAction)cancelBtnPressed:(id)sender {
    
    if ([self.actionDelegate respondsToSelector:@selector(dismissViewController)]) {
        
        [self.actionDelegate dismissViewController];
    }
}

- (IBAction)userLocationBtnPressed:(id)sender {
    
    [self.geocoder reverseGeocodeLocation:[LocationManager getCurrentUserLocation] completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks lastObject];
        
        NSArray *lines = [placemark.addressDictionary valueForKey:@"FormattedAddressLines"];
        
        NSString *appStr = @"";
        
        for (NSString *string in lines) {
            appStr = [appStr stringByAppendingString:[NSString stringWithFormat:@" %@", string]];
        }
        
        EKStructuredLocation *strLocation = [EKStructuredLocation locationWithTitle:appStr];
        self.cellModel.cellText = appStr;
        self.event.structuredLocation = strLocation;
        
        if ([self.actionDelegate respondsToSelector:@selector(dismissViewController)]) {
            
            [self.actionDelegate dismissViewController];
        }
        
    }];
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

    CLLocation *userLocation = [LocationManager getCurrentUserLocation];
    MKCoordinateRegion region;
    float spanX = 0;
    float spanY = 0;
    region.center.latitude = userLocation.coordinate.latitude;
    region.center.longitude = userLocation.coordinate.longitude;
    region.span = MKCoordinateSpanMake(spanX, spanY);
    
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = [searchBar.text stringByReplacingCharactersInRange:range withString:text];
    request.region = region;
    
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
//        if (error != nil) {
//            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Map Error",nil)
//                                        message:[error localizedDescription]
//                                       delegate:nil
//                              cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil] show];
//            return;
//        }
        
        if ([response.mapItems count] == 0) {
//            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Results",nil)
//                                        message:nil
//                                       delegate:nil
//                              cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil] show];
            
            self.tableData = nil;
            [self.tableView reloadData];
            
            return;
            
        } else {
            
            self.tableData = nil;
            self.tableData = response.mapItems;
            [self.tableView reloadData];
        }
        
    }];
    
    return YES;
}


#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.tableData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 55;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return @"Location";
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 32;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LocationTableViewCell"];
    
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"LocationTableViewCell" owner:self options:nil][0];
    }
    
    [cell setCellData:[self.tableData objectAtIndex:indexPath.row]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MKMapItem *mkItem = [self.tableData objectAtIndex:indexPath.row];
    MKPlacemark *placeMark = [[MKPlacemark alloc] initWithPlacemark:mkItem.placemark];
    NSArray *lines = [placeMark.addressDictionary valueForKey:@"FormattedAddressLines"];
    
    NSString *appStr = @"";
    
    for (NSString *string in lines) {
        appStr = [appStr stringByAppendingString:[NSString stringWithFormat:@" %@", string]];
    }
    
    EKStructuredLocation *strLocation = [EKStructuredLocation locationWithMapItem:mkItem];
    self.cellModel.cellText = appStr;
    self.event.structuredLocation = strLocation;
    
    if ([self.actionDelegate respondsToSelector:@selector(dismissViewController)]) {
        
        [self.actionDelegate dismissViewController];
    }
}

@end
