//
//  CustomCalendarModell.m
//  calendar
//
//  Created by Andrey on 9/22/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "CustomCalendarModell.h"

@implementation CustomCalendarModell

- (instancetype) initWithModelWithCalendar:(EKCalendar *)calendar colour:(UIColor *)colour title:(NSString *)title {
    
    self = [super init];
    
    if (self) {
        
        self.calendar = calendar;
        self.colour = colour;
        self.colourTitle = title;
    }
    
    return self;
}

@end
