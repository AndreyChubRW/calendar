//
//  BaseViewController.swift
//  calendar
//
//  Created by forever on 8/17/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

import UIKit
//import CalendarLib

enum CalendarViewType : Int {
    case CalendarViewDayType = 0
    case CalendarViewWeekType = 1
    case CalendarViewMonthType = 2
    case CalendarViewYearType = 3
    case CalendarViewListType = 4
}

class CustomEventView: MGCStandardEventView {
    
    
}

class BaseViewController: UIViewController, CalendarViewControllerDelegate, YearViewControllerDelegate, WeekViewControllerDelegate, ActionProtocol {

    @IBOutlet weak var imgIconToday: UIImageView!
    @IBOutlet weak var imgIconDay: UIImageView!
    @IBOutlet weak var imgIconWeek: UIImageView!
    @IBOutlet weak var imgIconMonth: UIImageView!
    @IBOutlet weak var imgIconYear: UIImageView!
    @IBOutlet weak var imgIconList: UIImageView!
    
    @IBOutlet weak var lblTabbarToday: UILabel!
    @IBOutlet weak var lblTabbarDay: UILabel!
    @IBOutlet weak var lblTabbarWeek: UILabel!
    @IBOutlet weak var lblTabbarMonth: UILabel!
    @IBOutlet weak var lblTabbarYear: UILabel!
    @IBOutlet weak var lblTabbarList: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    //////////////////////////////////////////
    //Selected Tabbar Index
    var nTabarIndex: Int = 0
    var calendarViewController: CalendarViewController?
    var monthViewController: MonthViewController?
    var weekViewController: WeekViewController?
    var yearViewController: YearViewController?
    var dayViewController: DayViewController?
    var firstTimeAppears:Bool?
    var calendar:NSCalendar?
    var dateFormatter:NSDateFormatter?
//    var datasource:[EAEvent]?
    var eventStore:EKEventStore?
    var calendarChooser:EKCalendarChooser?
    //////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.initData()
    }
    
    func initData() {
        
        self.resetTabbarIconImage()
        
        imgIconDay.image = UIImage(named:  "icon_day_select")
        lblTabbarDay.textColor = AppSetting.colorize(AppSetting.BG_COLOR)
        nTabarIndex = CalendarViewType.CalendarViewDayType.rawValue
        
        // Display SettingViewController
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(ShowSettingScreen),
            name: "ShowSettingScreen",
            object: nil)
        
        let calID = NSUserDefaults.standardUserDefaults().stringForKey("calendarIdentifier")
        self.calendar = NSCalendar.mgc_calendarFromPreferenceString(calID)
        let firstWeekday = NSUserDefaults.standardUserDefaults().objectForKey("firstDay") as? Int
        if firstWeekday != nil{
            if (firstWeekday != 0) {
                self.calendar!.firstWeekday = firstWeekday!
            }
        }
        else
        {
            calendar!.firstWeekday = 1
            NSUserDefaults.standardUserDefaults().registerDefaults(["firstDay": self.calendar!.firstWeekday])
        }
        
        dateFormatter = NSDateFormatter()
        dateFormatter!.calendar = self.calendar
//        self.calendar = cal
        
        let controller:CalendarViewController = self.controllerForViewType(CalendarViewType.CalendarViewDayType)
        self.nTabarIndex = CalendarViewType.CalendarViewDayType.rawValue
        
        self.addChildViewController(controller as! UIViewController)
        self.containerView.addSubview((controller as! UIViewController).view)
        (controller as! UIViewController).view.frame = self.containerView.bounds
        (controller as! UIViewController).didMoveToParentViewController(self)
        
        self.calendarViewController = controller
        self.firstTimeAppears = true
        
        //SetNavigationTitle
        let date = NSDate()
        self.SetNavigationTitle(date)
        
        /////////////////////////////////////
    }

    /////////////////////////////////
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.firstTimeAppears != nil {
            let date = self.calendar?.mgc_startOfWeekForDate(NSDate())
            self.calendarViewController?.moveToDate(date!, animated: false)
//            self.calendarViewController!.moveToDate(NSDate(), animated: false)
            self.firstTimeAppears = false
        }
        /*
        if self.firstTimeAppears != nil
        {
            let now = NSDate()
            let currentDateComponents = calendar!.components([.YearForWeekOfYear, .WeekOfYear ], fromDate: now)
            let startOfWeek = calendar!.dateFromComponents(currentDateComponents)
            if startOfWeek != nil{
                self.calendarViewController!.moveToDate(startOfWeek!, animated:false)
            }
            self.firstTimeAppears = false;
        }*/
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
//        let nc = (self.presentedViewController as! UINavigationController)
//        
//        let hide = (self.traitCollection.verticalSizeClass == .Regular && self.traitCollection.horizontalSizeClass == .Regular)
//        let doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(self.dismissSettings))
//        nc.topViewController!.navigationItem.rightBarButtonItem = hide ? nil : doneButton
    }
    
    func dismissSettings(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: { _ in })
    }
    
    func SetNavigationTitle(date : NSDate) {
        let calendar = self.calendar
        let components = calendar!.components([.Weekday, .Day , .Month , .Year], fromDate: date)
        
        let year =  components.year
        let month = components.month
        let day = components.day
        let weekDay = components.weekday
        
        //Change NavigationBar Title
        var strNavTitle = ""
        
        if self.nTabarIndex == CalendarViewType.CalendarViewYearType.rawValue {
            strNavTitle = String(format: "%d", year)
        } else if self.nTabarIndex == CalendarViewType.CalendarViewMonthType.rawValue {
            strNavTitle = String(format: "%@, %d", self.getMonthWeekString(month, bFlag: true), year)
        } else if self.nTabarIndex == CalendarViewType.CalendarViewWeekType.rawValue {
            strNavTitle = String(format: "%@, %d", self.getMonthWeekString(month, bFlag: true), year)
        } else if nTabarIndex == CalendarViewType.CalendarViewDayType.rawValue {
            strNavTitle = String(format: "%@, %@ %d, %d", self.getMonthWeekString(weekDay, bFlag: false), self.getMonthWeekString(month, bFlag: true), day, year)
        } else if nTabarIndex == CalendarViewType.CalendarViewListType.rawValue {
            
        }
        
        self.title = strNavTitle
    }
    
    func controllerForViewType(type:CalendarViewType) ->  CalendarViewController
    {
        
        switch type
        {
        case .CalendarViewDayType:
            return getDayViewController()
        case .CalendarViewListType:
            return getListViewController()
        case .CalendarViewWeekType:
            return getWeekViewController()
        case .CalendarViewMonthType:
            return getMonthViewController()
        case .CalendarViewYearType:
            return getYearViewController()
        }

    }
    
    func getListViewController() -> CalendarViewController {
        if self.yearViewController == nil {
            self.yearViewController = YearViewController()
            self.yearViewController!.calendar = self.calendar
            self.yearViewController!.delegate = self
        }
        return self.yearViewController!
    }
    
    func getYearViewController()-> YearViewController
    {
        if self.yearViewController == nil {
            self.yearViewController = YearViewController()
            self.yearViewController!.calendar = self.calendar
            self.yearViewController!.delegate = self
        }
        return self.yearViewController!
    }
    
    func getMonthViewController()-> MonthViewController
    {
        if self.monthViewController == nil {
            self.monthViewController = MonthViewController(eventStore: self.eventStore)
            self.monthViewController!.calendar = self.calendar
            self.monthViewController!.delegate = self
        }
        else
        {
            self.monthViewController!.reloadEvents()
        }
        return self.monthViewController!
    }
    
    func getWeekViewController()-> WeekViewController
    {
        if self.weekViewController == nil {
            self.weekViewController = WeekViewController(eventStore: self.eventStore)
            (self.weekViewController! as MGCDayPlannerEKViewController).calendar = self.calendar
            self.weekViewController!.delegate = self        }
        return self.weekViewController!
    }
    
    func getDayViewController()-> DayViewController
    {
        if self.dayViewController == nil {
            self.dayViewController = DayViewController(eventStore: self.eventStore)
            (self.dayViewController! as MGCDayPlannerEKViewController).calendar = self.calendar
            self.dayViewController!.showsWeekHeaderView = true;
            self.dayViewController!.delegate = self
        }
        return self.dayViewController!
    }
    
    
    func calendarViewController(controller: CalendarViewController, didSelectEvent event: AnyObject)
    {
        
    }
    
    func calendarViewController(controller: CalendarViewController, didShowDate date: NSDate)
    {
        if controller.isKindOfClass(YearViewController)
        {
            self.dateFormatter?.dateFormat = "yyyy"
        }
        else
        {
            self.dateFormatter?.dateFormat = "MMMM yyyy"
        }
        
        self.SetNavigationTitle(date)
//        let str = self.dateFormatter!.stringFromDate(date)
    }
    
    func moveToNewController(newController:CalendarViewController, atDate:NSDate)
    {
        let currentController = self.calendarViewController as! UIViewController
        let nextController = newController as! UIViewController
        currentController.willMoveToParentViewController(nil)
        self.addChildViewController(nextController)
        
        self.transitionFromViewController(currentController, toViewController: nextController, duration: 0.5, options: .TransitionCrossDissolve, animations: {() -> Void in
            nextController.view.frame = self.containerView.bounds
            nextController.view.hidden = true
            }, completion: {(finished: Bool) -> Void in
                currentController.removeFromParentViewController()
                nextController.didMoveToParentViewController(self)
                self.calendarViewController = newController
                newController.moveToDate(atDate, animated: false)
                nextController.view.hidden = false
        })
        
    }
    
    func yearViewController(controller: YearViewController, didSelectMonthAtDate date: NSDate) {
        let controllerNew:CalendarViewController = self.controllerForViewType(.CalendarViewMonthType)
        
        self.moveToNewController(controllerNew, atDate: date)
        self.nTabarIndex = CalendarViewType.CalendarViewMonthType.rawValue;
        
        self.resetTabbarIconImage()
        
        imgIconMonth.image = UIImage(named: "icon_month_select")
        lblTabbarMonth.textColor = AppSetting.colorize(AppSetting.BG_COLOR)
    }
    
    /////////////////////////////////
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func resetTabbarIconImage() {
        imgIconToday.image  = UIImage(named: "icon_today")
        imgIconDay.image    = UIImage(named: "icon_day")
        imgIconWeek.image   = UIImage(named: "icon_week")
        imgIconMonth.image  = UIImage(named: "icon_month")
        imgIconYear.image   = UIImage(named: "icon_year")
        imgIconList.image   = UIImage(named: "icon_list")
        
        lblTabbarToday.textColor    = UIColor.grayColor()
        lblTabbarDay.textColor      = UIColor.grayColor()
        lblTabbarWeek.textColor     = UIColor.grayColor()
        lblTabbarMonth.textColor    = UIColor.grayColor()
        lblTabbarYear.textColor     = UIColor.grayColor()
        lblTabbarList.textColor     = UIColor.grayColor()
    }
    
    func ShowSettingScreen() {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SettingVC") as! SettingViewController
        self.navigationController?.presentViewController(vc, animated: true, completion: { 
            let sideVC = (UIApplication.sharedApplication().delegate as! AppDelegate).sideVC
            sideVC?.LeftPanelShouldExpand()
        })
    }
    
    func getMonthWeekString(value:Int, bFlag:Bool) -> String {
        
        //bFlag == 1 : Month, bFlag == 0 : Week
        var str = ""
        if bFlag == true {
            switch value {
            case 1:
                str = "Jan"
                break
            case 2:
                str = "Feb"
                break
            case 3:
                str = "Mar"
                break
            case 4:
                str = "Apr"
                break
            case 5:
                str = "May"
                break
            case 6:
                str = "Jun"
                break
            case 7:
                str = "Jul"
                break
            case 8:
                str = "Aug"
                break
            case 9:
                str = "Sep"
                break
            case 10:
                str = "Oct"
                break
            case 11:
                str = "Nov"
                break
            case 12:
                str = "Dec"
                break
            default:
                str = ""
            }
        } else {
            switch value {
            case 1:
                str = "Sun"
                break
            case 2:
                str = "Mon"
                break
            case 3:
                str = "Tue"
                break
            case 4:
                str = "Wed"
                break
            case 5:
                str = "Thu"
                break
            case 6:
                str = "Fri"
                break
            case 7:
                str = "Sat"
                break
            default:
                str = ""
            }
        }
        
        return str;
    }
    
    func switchControllers(index: Int) {
        
        if self.nTabarIndex == index {
            return
        } else {
            self.nTabarIndex = index
        }
        
        let controller = self.controllerForViewType(CalendarViewType(rawValue: index)!)
        self.moveToNewController(controller, atDate:NSDate())
        self.calendarViewController!.moveToDate(NSDate(), animated: true)
    }
    
    
    @IBAction func onAdd(sender: AnyObject) {
        
//        let eventController = CustomEventEditViewController.init(customEventEditViewController: ())
//        eventController.dismissDelegate = self;
//        eventController.eventKitSupport = MGCEventKitSupport.init(eventStore: self.eventStore)
        
//        self.navigationController?.navigationBarHidden = true
//        self.showDetailViewController(eventController, sender: self)
    }
    
    @IBAction func onMenu(sender: AnyObject) {
        if let sideVC = (UIApplication.sharedApplication().delegate as! AppDelegate).sideVC {
            sideVC.isCollapsed() ? sideVC.expandLeftPanel() : sideVC.collapsePanel()
        }
    }
    
    @IBAction func onToday(sender: UIButton) {
        self.calendarViewController!.moveToDate(NSDate(), animated: true)
//        self.resetTabbarIconImage()
//        
//        imgIconToday.image = UIImage(named: "icon_today_select")
//        lblTabbarToday.textColor = AppSetting.colorize(AppSetting.BG_COLOR)

    }
    
    @IBAction func onDay(sender: AnyObject) {
        self.resetTabbarIconImage()
        
        imgIconDay.image = UIImage(named: "icon_day_select")
        lblTabbarDay.textColor = AppSetting.colorize(AppSetting.BG_COLOR)
        switchControllers(CalendarViewType.CalendarViewDayType.rawValue)
    }
    
    @IBAction func onWeek(sender: AnyObject) {
        self.resetTabbarIconImage()
        
        imgIconWeek.image = UIImage(named: "icon_week_select")
        lblTabbarWeek.textColor = AppSetting.colorize(AppSetting.BG_COLOR)
        switchControllers(CalendarViewType.CalendarViewWeekType.rawValue)
    }
    
    @IBAction func onMonth(sender: AnyObject) {
        self.resetTabbarIconImage()
        
        imgIconMonth.image = UIImage(named: "icon_month_select")
        lblTabbarMonth.textColor = AppSetting.colorize(AppSetting.BG_COLOR)
        switchControllers(CalendarViewType.CalendarViewMonthType.rawValue)
    }
    
    @IBAction func onYear(sender: AnyObject) {
        self.resetTabbarIconImage()
        
        imgIconYear.image = UIImage(named: "icon_year_select")
        lblTabbarYear.textColor = AppSetting.colorize(AppSetting.BG_COLOR)
        switchControllers(CalendarViewType.CalendarViewYearType.rawValue)
    }
    
    @IBAction func onList(sender: AnyObject) {
        self.resetTabbarIconImage()
        
        imgIconList.image = UIImage(named: "icon_list_select")
        lblTabbarList.textColor = AppSetting.colorize(AppSetting.BG_COLOR)
        switchControllers(CalendarViewType.CalendarViewYearType.rawValue)
    }
    
    func dismissViewController() {
        
        self.navigationController?.navigationBarHidden = false
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
