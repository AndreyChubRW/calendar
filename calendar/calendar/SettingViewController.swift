//
//  SettingViewController.swift
//  calendar
//
//  Created by forever on 8/20/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate {

    var cellType : Int!
    var aryPickerView : NSMutableArray!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        aryPickerView = NSMutableArray()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onDone(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    //UITableViewDelegate & UITableViewDataSoure Method
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "CALENDAR"
        } else if section == 1 {
            return "DATE & TIME"
        } else {
            return "SUPPORT"
        }
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let title = UILabel()
        title.textAlignment = NSTextAlignment.Center
        
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textAlignment = title.textAlignment
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
            return 1
        } else {
            return 2
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("SettingCalendarTableViewCell") as! SettingCalendarTableViewCell
            if indexPath.row == 0 {
                (cell as! SettingCalendarTableViewCell).lblTitle.text = "Sync To Google Calendar"
            } else {
                (cell as! SettingCalendarTableViewCell).lblTitle.text = "Sync To iCal"
            }
        } else if indexPath.section == 1 {
            cell = tableView.dequeueReusableCellWithIdentifier("SettingDateTableViewCell") as! SettingDateTableViewCell
            
            if indexPath.row == 0 {
                (cell as! SettingDateTableViewCell).lblTitle.text = "Time Zone"
                (cell as! SettingDateTableViewCell).lblDataAndTime.text = "Asia/Tokyu"
            } else {
                (cell as! SettingDateTableViewCell).lblTitle.text = "Week Start On"
                (cell as! SettingDateTableViewCell).lblDataAndTime.text = "Monday"
            }
            
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier("SettingSupportTableViewCell")
        }
        
        cell.tintColor = UIColor.whiteColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
            
        } else if indexPath.section == 1 {
            print("TimeZone Selected")
            resetPickerData(indexPath.row)
        } else if indexPath.section == 2 {
            print("Support Selected")
        }
    }
    
    func resetPickerData(index: Int) {
    
        aryPickerView.removeAllObjects()
        
        if index == 0 {
            
        } else {
            aryPickerView.addObjectsFromArray(["Monday", "Tuesday", "Wednesday", "ThusDay", "Friday", "Saturday", "Sunday"])
            pickerView.reloadAllComponents()
        }
        
        UIView.animateWithDuration(0.3, animations: {
            self.pickerView.frame = CGRectMake(0, self.view.frame.size.height - 200, self.view.frame.size.width, 200)
        })
    }
    //PickerView Delegate Method
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return aryPickerView.count
    }

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return aryPickerView[row] as? String
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if(row == 0)
        {
            self.view.backgroundColor = UIColor.whiteColor();
        }
        else if(row == 1)
        {
            self.view.backgroundColor = UIColor.redColor();
        }
        else if(row == 2)
        {
            self.view.backgroundColor =  UIColor.greenColor();
        }
        else
        {
            self.view.backgroundColor = UIColor.blueColor();
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
