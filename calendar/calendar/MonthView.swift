//
//  MonthView.swift
//  calendar
//
//  Created by forever on 8/17/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

import UIKit
//import CalendarLib

class MonthView:  UIView, MGCMonthPlannerViewDelegate
{
    
    func monthPlannerView(view: MGCMonthPlannerView!, attributedStringForDayHeaderAtDate date: NSDate!) -> NSAttributedString! {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "d";
        let dayStr = dateFormatter.stringFromDate(date)
        
        var str = dayStr;
        
        if (Int(dayStr) == 1) {
            dateFormatter.dateFormat = "MMM d";
            str = dateFormatter.stringFromDate(date)
        }
        
        let font = UIFont.systemFontOfSize(15)
        let attrStr = NSMutableAttributedString(string: str, attributes: [ NSFontAttributeName: font ])
        
        // if ([self.calendar mgc_isDate:date sameDayAsDate:[NSDate date]]) {
        if date == NSDate()
        {
            let boldFont = UIFont.boldSystemFontOfSize(15)
            
            let mark = MGCCircleMark()
            mark.yOffset = boldFont.descender - mark.margin;
            
            //let theRangeText = str.rangeOfString(dayStr)
            let start = str.startIndex.distanceTo(dayStr.startIndex)
            let length = dayStr.startIndex.distanceTo( dayStr.endIndex)
            let range = NSMakeRange(start, length)
            attrStr.addAttributes([NSFontAttributeName: boldFont, NSForegroundColorAttributeName: UIColor.whiteColor(), MGCCircleMarkAttributeName: mark], range: range)
            
            attrStr.processCircleMarksInRange(NSMakeRange(0, attrStr.length))
        }
        
        let para = NSMutableParagraphStyle()
        para.alignment = NSTextAlignment.Right;
        para.tailIndent = -6;
        
        attrStr.addAttributes([NSParagraphStyleAttributeName: para], range: NSMakeRange(0, attrStr.length))
        return attrStr
    }
    
    func monthPlannerView(view: MGCMonthPlannerView!, didSelectDayCellAtDate date: NSDate!) {
        
    }
    
    func monthPlannerView(view: MGCMonthPlannerView!, didSelectEventAtIndex index: UInt, date: NSDate!) {
        
    }
    
    func monthPlannerView(view: MGCMonthPlannerView!, willStartMovingEventAtIndex index: UInt, date: NSDate!) {
        
    }
    
    func monthPlannerView(view: MGCMonthPlannerView!, didDeselectEventAtIndex index: UInt, date: NSDate!) {
        
    }
    func monthPlannerView(view: MGCMonthPlannerView!, shouldDeselectEventAtIndex index: UInt, date: NSDate!) -> Bool {
        return true
    }
    
    func monthPlannerView(view: MGCMonthPlannerView!, shouldSelectEventAtIndex index: UInt, date: NSDate!) -> Bool {
        return true
    }
    
    func monthPlannerView(view: MGCMonthPlannerView!, didShowCell cell: MGCEventView!, forNewEventAtDate date: NSDate!) {
        
    }
    
    func monthPlannerView(view: MGCMonthPlannerView!, didMoveEventAtIndex index: UInt, date dateOld: NSDate!, toDate dayNew: NSDate!) {
        
    }
    
    func monthPlannerViewDidScroll(view: MGCMonthPlannerView!) {
        /*
         super.monthPlannerViewDidScroll(view)
         var date: NSDate = self.monthPlannerView.dayAtPoint(self.monthPlannerView.center)
         if date && self.delegate.respondsToSelector("calendarViewController:didShowDate:") {
         self.delegate.calendarViewController(self, didShowDate: date)
         }
         */
        
    }
    

}