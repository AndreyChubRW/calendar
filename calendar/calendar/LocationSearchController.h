//
//  LocationSearchController.h
//  calendar
//
//  Created by Andrey on 9/21/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomEventEditViewController.h"
#import <MapKit/MapKit.h>
#import "LocationManager.h"
#import "TableViewCellModel.h"

@interface LocationSearchController : UIViewController

@property (weak, nonatomic) id <ActionProtocol> actionDelegate;

- (instancetype)initLocationSearchControllerWithModel:(TableViewCellModel *)cellModel event:(EKEvent *)event;

@end
