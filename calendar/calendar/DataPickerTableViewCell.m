//
//  DataPickerTableViewCell.m
//  calendar
//
//  Created by Andrey on 9/16/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "DataPickerTableViewCell.h"

@interface DataPickerTableViewCell ()

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (strong, nonatomic) NSString *kinde;

@end

@implementation DataPickerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCellData:(TableViewCellModel *)cellModel {
    
    self.kinde = cellModel.kind;
}

- (IBAction)datePikerChangeDate:(id)sender {
    
    if ([self.actionDelegate respondsToSelector:@selector(changePickerDate: forKind:)]) {
        
        [self.actionDelegate changePickerDate:self.datePicker.date forKind:self.kinde];
    }
}


@end
