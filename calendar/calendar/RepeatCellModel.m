//
//  RepeatCellModel.m
//  calendar
//
//  Created by Andrey on 9/21/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "RepeatCellModel.h"

@implementation RepeatCellModel

- (instancetype)initWithTitle:(NSString *)title date:(NSDate *)date state:(BOOL)isSelected {
    
    self = [super init];
    
    if (self) {
        
        self.title = title;
        self.date = date;
        self.isSelected = isSelected;
    }
    return self;
}

@end
