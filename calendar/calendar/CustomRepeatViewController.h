//
//  CustomRepeatViewController.h
//  calendar
//
//  Created by Andrey on 9/21/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import "CustomEventEditViewController.h"
#import "TableViewCellModel.h"

@interface CustomRepeatViewController : UIViewController

@property (strong, nonatomic) TableViewCellModel *repeatCell;
@property (weak, nonatomic) id <ActionProtocol> actionDelegate;

- (instancetype)initCustomRepeatViewControllerWithEvent:(EKEvent *)event;

@end
