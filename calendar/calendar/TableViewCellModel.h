//
//  TableViewCellModel.h
//  calendar
//
//  Created by Andrey on 9/17/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TableViewCellModel : NSObject

@property (copy, nonatomic) NSString *pictureName;
@property (copy, nonatomic) NSString *placeHolder;
@property (copy, nonatomic) NSString *identifier;
@property (copy, nonatomic) NSNumber *height;
@property (copy, nonatomic) NSString *cellText;
@property (copy, nonatomic) NSDate * date;
@property (copy, nonatomic) NSDate * eventDate;
@property (assign, nonatomic) BOOL dropDown;
@property (assign, nonatomic) BOOL isOpen;
@property (copy, nonatomic) NSArray *dropCell;
@property (copy, nonatomic) NSString *kind;

- (instancetype)initWithImageName:(NSString *)pictureName placeHolder:(NSString *)placeHolder cellText:(NSString *)cellText identifier:(NSString *)identifier height:(NSNumber *)height;

@end
