//
//  CustomEventEditViewControllerCell.h
//  calendar
//
//  Created by Andrey on 9/16/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellModel.h"
#import <EventKit/EventKit.h>

@interface CustomEventEditViewControllerCell : UITableViewCell

@property (strong, nonatomic) EKEvent *event;
- (void)setCellData:(TableViewCellModel *)dict;

@end
