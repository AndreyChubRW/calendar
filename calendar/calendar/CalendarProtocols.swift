//
//  CalendarProtocols.swift
//  calendar
//
//  Created by forever on 8/17/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//
import UIKit
//import CalendarLib

protocol CalendarViewControllerNavigation
{
    var centerDate: NSDate? {get}


    func moveToDate(date: NSDate, animated: Bool)
    func moveToNextPageAnimated(animated: Bool)
    func moveToPreviousPageAnimated(animated: Bool)

}

typealias CalendarViewController = protocol<NSObjectProtocol, CalendarViewControllerNavigation>

protocol CalendarViewControllerDelegate
{
    func calendarViewController(controller: CalendarViewController, didShowDate date: NSDate)
    func calendarViewController(controller: CalendarViewController, didSelectEvent event: AnyObject)
}
