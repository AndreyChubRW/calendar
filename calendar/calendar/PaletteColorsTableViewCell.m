//
//  PaletteColorsTableViewCell.m
//  calendar
//
//  Created by Andrey on 9/16/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "PaletteColorsTableViewCell.h"

@interface PaletteColorsTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *topRowImage1ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *topRowImage2ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *topRowImage3ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *topRowImage4ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *topRowImage5ImageView;

@property (weak, nonatomic) IBOutlet UIButton *topRowBtn1;
@property (weak, nonatomic) IBOutlet UIButton *topRowBtn2;
@property (weak, nonatomic) IBOutlet UIButton *topRowBtn3;
@property (weak, nonatomic) IBOutlet UIButton *topRowBtn4;
@property (weak, nonatomic) IBOutlet UIButton *topRowBtn5;

@property (weak, nonatomic) IBOutlet UIImageView *bottomRowImage1ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomRowImage2ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomRowImage3ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomRowImage4ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomRowImage5ImageView;

@property (weak, nonatomic) IBOutlet UIButton *bottomRowBtn1;
@property (weak, nonatomic) IBOutlet UIButton *bottomRowBtn2;
@property (weak, nonatomic) IBOutlet UIButton *bottomRowBtn3;
@property (weak, nonatomic) IBOutlet UIButton *bottomRowBtn4;
@property (weak, nonatomic) IBOutlet UIButton *bottomRowBtn5;

@property (strong, nonatomic) NSArray *topRowImagesArray;
@property (strong, nonatomic) NSArray *bottomRowImagesArray;

@property (strong, nonatomic) NSArray *topColorArray;
@property (strong, nonatomic) NSArray *bottomColorArray;

@property (strong, nonatomic) NSArray *topRowBtnArray;
@property (strong, nonatomic) NSArray *bottomRowBtnArray;

@property (strong, nonatomic) NSArray *borderColorTopImages;
@property (strong, nonatomic) NSArray *borderColorBottomImages;

@end

@implementation PaletteColorsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCellData {
    
    self.topRowBtnArray = @[self.topRowBtn1, self.topRowBtn2, self.topRowBtn3, self.topRowBtn4, self.topRowBtn5];
    self.bottomRowBtnArray = @[self.bottomRowBtn1, self.bottomRowBtn2, self.bottomRowBtn3, self.bottomRowBtn4, self.bottomRowBtn5];
    
    self.topRowImagesArray = @[self.topRowImage1ImageView, self.topRowImage2ImageView,self.topRowImage3ImageView,self.topRowImage4ImageView,self.topRowImage5ImageView];
    self.bottomRowImagesArray = @[self.bottomRowImage1ImageView,self.bottomRowImage2ImageView,self.bottomRowImage3ImageView,self.bottomRowImage4ImageView,self.bottomRowImage5ImageView];
    
    [self setPalletColor];
}


#pragma mark - Action 

- (IBAction)topRowBtnPressed1:(id)sender {
    
    [self getSelectedCalendarAtIndex:0];
    [self getSelectedColorFromCollorArray:self.borderColorTopImages forBtn:sender fromBtnArray:self.topRowBtnArray];
}

- (IBAction)topRowBtnPressed2:(id)sender {
    
    [self getSelectedCalendarAtIndex:1];
    [self getSelectedColorFromCollorArray:self.borderColorTopImages forBtn:sender fromBtnArray:self.topRowBtnArray];
}

- (IBAction)topRowBtnPressed3:(id)sender {
    
    [self getSelectedCalendarAtIndex:2];
    [self getSelectedColorFromCollorArray:self.borderColorTopImages forBtn:sender fromBtnArray:self.topRowBtnArray];
}

- (IBAction)topRowBtnPressed4:(id)sender {
    
    [self getSelectedCalendarAtIndex:3];
    [self getSelectedColorFromCollorArray:self.borderColorTopImages forBtn:sender fromBtnArray:self.topRowBtnArray];
}

- (IBAction)topRowBtnPressed5:(id)sender {
    
    [self getSelectedCalendarAtIndex:4];
    [self getSelectedColorFromCollorArray:self.borderColorTopImages forBtn:sender fromBtnArray:self.topRowBtnArray];
}

- (IBAction)bottomRowBtnPressed1:(id)sender {
    
    [self getSelectedCalendarAtIndex:5];
    [self getSelectedColorFromCollorArray:self.borderColorBottomImages forBtn:sender fromBtnArray:self.bottomRowBtnArray];
}

- (IBAction)bottomRowBtnPressed2:(id)sender {
    
    [self getSelectedCalendarAtIndex:6];
    [self getSelectedColorFromCollorArray:self.borderColorBottomImages forBtn:sender fromBtnArray:self.bottomRowBtnArray];
}

- (IBAction)bottomRowBtnPressed3:(id)sender {
    
    [self getSelectedCalendarAtIndex:7];
    [self getSelectedColorFromCollorArray:self.borderColorBottomImages forBtn:sender fromBtnArray:self.bottomRowBtnArray];
}

- (IBAction)bottomRowBtnPressed4:(id)sender {
    
    [self getSelectedCalendarAtIndex:8];
    [self getSelectedColorFromCollorArray:self.borderColorBottomImages forBtn:sender fromBtnArray:self.bottomRowBtnArray];
}

- (IBAction)bottomRowBtnPressed5:(id)sender {
    
    [self getSelectedCalendarAtIndex:9];
    [self getSelectedColorFromCollorArray:self.borderColorBottomImages forBtn:sender fromBtnArray:self.bottomRowBtnArray];
}

- (void)getSelectedCalendarAtIndex:(NSUInteger )index {
    
    if ([self.actionDelegate respondsToSelector:@selector(changePalletColour:)]) {
        
        [self.actionDelegate changePalletColour:[[CustomCalendarManager sharedInstance] getCalendarAtIndex:index]];
    }

}


- (void)getSelectedColorFromCollorArray:(NSArray *)colorArray forBtn:(UIButton *)sender fromBtnArray:(NSArray *)btnArray {
        
    for (UIButton *btn in self.topRowBtnArray) {
        
        [btn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    for (UIButton *btn in self.bottomRowBtnArray) {
        
        [btn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    
    [sender setImage:[UIImage imageNamed:@"selectedColorCheckMark"] forState:UIControlStateNormal];
}

- (void)setPalletColor {
    
    UIColor *color1 = [UIColor colorWithRed:(CGFloat)(253.f/255.f) green:(CGFloat)(219.f/255.f) blue:(CGFloat)(225.f/255.f) alpha:1];
    UIColor *color2 = [UIColor colorWithRed:(CGFloat)(255.f/255.f) green:(CGFloat)(210.f/255.f) blue:(CGFloat)(162.f/255.f) alpha:1];
    UIColor *color3 = [UIColor colorWithRed:(CGFloat)(255.f/255.f) green:(CGFloat)(244.f/255.f) blue:(CGFloat)(169.f/255.f) alpha:1];
    UIColor *color4 = [UIColor colorWithRed:(CGFloat)(168.f/255.f) green:(CGFloat)(255.f/255.f) blue:(CGFloat)(174.f/255.f) alpha:1];
    UIColor *color5 = [UIColor colorWithRed:(CGFloat)(211.f/255.f) green:(CGFloat)(225.f/255.f) blue:(CGFloat)(255.f/255.f) alpha:1];
    UIColor *color6 = [UIColor colorWithRed:(CGFloat)(189.f/255.f) green:(CGFloat)(243.f/255.f) blue:(CGFloat)(255.f/255.f) alpha:1];
    UIColor *color7 = [UIColor colorWithRed:(CGFloat)(219.f/255.f) green:(CGFloat)(238.f/255.f) blue:(CGFloat)(140.f/255.f) alpha:1];
    UIColor *color8 = [UIColor colorWithRed:(CGFloat)(161.f/255.f) green:(CGFloat)(248.f/255.f) blue:(CGFloat)(231.f/255.f) alpha:1];
    UIColor *color9 = [UIColor colorWithRed:(CGFloat)(255.f/255.f) green:(CGFloat)(190.f/255.f) blue:(CGFloat)(190.f/255.f) alpha:1];
    UIColor *color10 = [UIColor colorWithRed:(CGFloat)(255.f/255.f) green:(CGFloat)(209.f/255.f) blue:(CGFloat)(249.f/255.f) alpha:1];
    
    UIColor *borderColor1 = [UIColor colorWithRed:(CGFloat)(223.f/255.f) green:(CGFloat)(157.f/255.f) blue:(CGFloat)(168.f/255.f) alpha:1];
    UIColor *borderColor2 = [UIColor colorWithRed:(CGFloat)(202.f/255.f) green:(CGFloat)(127.f/255.f) blue:(CGFloat)(76.f/255.f) alpha:1];
    UIColor *borderColor3 = [UIColor colorWithRed:(CGFloat)(186.f/255.f) green:(CGFloat)(168.f/255.f) blue:(CGFloat)(42.f/255.f) alpha:1];
    UIColor *borderColor4 = [UIColor colorWithRed:(CGFloat)(117.f/255.f) green:(CGFloat)(192.f/255.f) blue:(CGFloat)(65.f/255.f) alpha:1];
    UIColor *borderColor5 = [UIColor colorWithRed:(CGFloat)(131.f/255.f) green:(CGFloat)(158.f/255.f) blue:(CGFloat)(216.f/255.f) alpha:1];
    UIColor *borderColor6 = [UIColor colorWithRed:(CGFloat)(71.f/255.f) green:(CGFloat)(170.f/255.f) blue:(CGFloat)(192.f/255.f) alpha:1];
    UIColor *borderColor7 = [UIColor colorWithRed:(CGFloat)(156.f/255.f) green:(CGFloat)(182.f/255.f) blue:(CGFloat)(49.f/255.f) alpha:1];
    UIColor *borderColor8 = [UIColor colorWithRed:(CGFloat)(55.f/255.f) green:(CGFloat)(193.f/255.f) blue:(CGFloat)(166.f/255.f) alpha:1];
    UIColor *borderColor9 = [UIColor colorWithRed:(CGFloat)(217.f/255.f) green:(CGFloat)(124.f/255.f) blue:(CGFloat)(105.f/255.f) alpha:1];
    UIColor *borderColor10 = [UIColor colorWithRed:(CGFloat)(216.f/255.f) green:(CGFloat)(113.f/255.f) blue:(CGFloat)(203.f/255.f) alpha:1];
    
    self.topColorArray = @[color1,color2,color3,color4,color5];
    self.bottomColorArray = @[color6,color7,color8,color9,color10];
    
    self.borderColorTopImages = @[borderColor1, borderColor2, borderColor3, borderColor4, borderColor5];
    self.borderColorBottomImages = @[borderColor6, borderColor7, borderColor8, borderColor9, borderColor10];
    
    for (UIImageView *imView in self.topRowImagesArray) {
        NSUInteger index = [self.topRowImagesArray indexOfObject:imView];
        
        imView.backgroundColor = self.topColorArray[index];
        imView.layer.cornerRadius = 16;
    }
    
    for (UIImageView *imView in self.bottomRowImagesArray) {
        NSUInteger index = [self.bottomRowImagesArray indexOfObject:imView];
        
        imView.backgroundColor = self.bottomColorArray[index];
        imView.layer.cornerRadius = 16;
    }
    
    for (UIImageView *imView in self.topRowImagesArray) {
        NSUInteger index = [self.topRowImagesArray indexOfObject:imView];
        
        UIColor *bColor = self.borderColorTopImages[index];
        imView.layer.borderColor = bColor.CGColor;
        imView.layer.borderWidth = 1;
    }
    
    for (UIImageView *imView in self.bottomRowImagesArray) {
        NSUInteger index = [self.bottomRowImagesArray indexOfObject:imView];
        
        UIColor *bColor = self.borderColorBottomImages[index];
        imView.layer.borderColor = bColor.CGColor;
        imView.layer.borderWidth = 1;
    }
}

@end
