//
//  EndRepeatViewController.m
//  calendar
//
//  Created by Andrey on 9/22/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "EndRepeatViewController.h"

@interface EndRepeatViewController ()

@property (strong, nonatomic) EKEvent *event;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *neverBtn;
@property (weak, nonatomic) IBOutlet UIButton *onDateBtn;
@property (weak, nonatomic) IBOutlet UIImageView *neverImageView;
@property (weak, nonatomic) IBOutlet UIImageView *onDateImageView;
@property (weak, nonatomic) IBOutlet UIView *bottomSeparatorView;

@end

@implementation EndRepeatViewController

- (instancetype)initEndRepeatViewControllerWithEvent:(EKEvent *)event {
    
    self = [[NSBundle mainBundle] loadNibNamed:@"EndRepeatViewController" owner:self options:nil][0];
    
    if (self) {
        
        self.event = event;
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    
    if ([self.endRepeatCell.cellText isEqualToString:@"Never"]) {
        self.neverImageView.image = [UIImage imageNamed:@"selectedColorCheckMark"];
        self.datePicker.hidden = YES;
        self.bottomSeparatorView.hidden = YES;
    }
    else {
        self.onDateImageView.image = [UIImage imageNamed:@"selectedColorCheckMark"];
        self.datePicker.hidden = NO;
        self.bottomSeparatorView.hidden = NO;
        
        self.datePicker.date = self.endRepeatCell.date;
    }
}

- (IBAction)saveBtn:(id)sender {
    
    if ([self.actionDelegate respondsToSelector:@selector(dismissViewController)]) {
        
        [self.actionDelegate dismissViewController];
    }
}

- (IBAction)neverBtnPressed:(id)sender {
    
    self.neverImageView.image = [UIImage imageNamed:@"selectedColorCheckMark"];
    self.onDateImageView.image = [UIImage imageNamed:@""];
    self.datePicker.hidden = YES;
    self.bottomSeparatorView.hidden = YES;
    self.event.recurrenceRules = nil;
    
    self.repeatCell.cellText = @"Never";
    self.endRepeatCell.cellText = @"Never";
    
}
- (IBAction)onDateBtnPressed:(id)sender {
    
    self.neverImageView.image = [UIImage imageNamed:@""];
    self.onDateImageView.image = [UIImage imageNamed:@"selectedColorCheckMark"];
    self.datePicker.hidden = NO;
    self.bottomSeparatorView.hidden = NO;
    
    NSDateFormatter *dateFomatter = [[NSDateFormatter alloc] init];
    [dateFomatter setDateFormat:@"E, MMM d, HH:mm"];
    self.endRepeatCell.cellText = [dateFomatter stringFromDate:self.datePicker.date];
    self.endRepeatCell.date = self.datePicker.date;
}

- (IBAction)datePickerChangeDate:(id)sender {
    
    EKRecurrenceRule *recurrenceFromEvent = [[EKRecurrenceRule alloc] init];
    recurrenceFromEvent = [self.event.recurrenceRules firstObject];
    
    EKRecurrenceEnd *recurrenceEnd = [EKRecurrenceEnd recurrenceEndWithEndDate:self.datePicker.date];
    
    EKRecurrenceRule *recurrenceNew = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:recurrenceFromEvent.frequency interval:recurrenceFromEvent.interval end:recurrenceEnd];
    
    NSDateFormatter *dateFomatter = [[NSDateFormatter alloc] init];
    [dateFomatter setDateFormat:@"E, MMM d, HH:mm"];
    self.endRepeatCell.cellText = [dateFomatter stringFromDate:self.datePicker.date];
    self.endRepeatCell.date = self.datePicker.date;
    
    self.event.recurrenceRules = nil;
    [self.event addRecurrenceRule:recurrenceNew];
}

@end
