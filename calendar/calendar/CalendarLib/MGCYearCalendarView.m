//
//  MGCYearCalendarView.m
//  Graphical Calendars Library for iOS
//
//  Distributed under the MIT License
//  Get the latest version from here:
//
//	https://github.com/jumartin/Calendar
//
//  Copyright (c) 2014-2015 Julien Martin
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#import "MGCYearCalendarView.h"
#import "MGCYearCalendarMonthCell.h"
#import "MGCMonthMiniCalendarView.h"
#import "MGCYearCalendarMonthHeaderView.h"
#import "NSCalendar+MGCAdditions.h"
#import "Constant.h"

// reuse identifiers for collection view cells and supplementary views
static NSString* const MonthCellReuseIdentifier = @"MonthCellReuseIdentifier";
static NSString* const YearHeaderReuseIdentifier = @"YearHeaderReuseIdentifier";

static const NSUInteger kYearsLoadingStep = 10;			// number of years in a loaded page = 2 * kYearsLoadingStep  + 1
static const CGFloat kCellMinimumSpacing = 25;			// minimum distance between month cells
static const CGFloat kDefaultDayFontSize = 13;			// default font size for the day ordinals
static const CGFloat kDefaultMonthHeaderFontSize = 20;	// default font size for the month headers
static const CGFloat kDefaultYearHeaderFontSize = 40;	// deafult font size for the year headers

static const CGFloat kCellMinimumSpacingiPhone = 3;			// minimum distance between month cells
static const CGFloat kDefaultDayFontSizeiPhone = 9;			// default font size for the day ordinals
static const CGFloat kDefaultMonthHeaderFontSizeiPhone = 15;	// default font size for the month headers
static const CGFloat kDefaultYearHeaderFontSizeiPhone = 20;	// deafult font size for the year headers


// forward declaration needed by YearEventsView
@interface MGCYearCalendarView(Scrolling)

- (BOOL)reloadCollectionViewIfNeeded;

@end

// YearEventsView: this is needed for infinite scrolling.
// It seems we only need this subclass of UICollectionView in iOS 6.
// With iOS 7, we can use delegate scrollView:didScroll to recenter the content and adjust the offset.
// In iOS 6, this causes mad (too fast) scrolling! Overriding layoutSubviews works though.
@interface YearEventsView : UICollectionView

@property (nonatomic) MGCYearCalendarView *yearView;

@end

@implementation YearEventsView

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.contentSize = [self.collectionViewLayout collectionViewContentSize];
    [self.yearView reloadCollectionViewIfNeeded];
}

@end

#pragma mark -

@interface MGCYearCalendarView ()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MGCMonthMiniCalendarViewDelegate>

@property (nonatomic, readonly) UIScrollView *verticalScrollView;
@property (nonatomic, readonly) YearEventsView *eventsView;		// collection view
@property (nonatomic, copy) NSDate *startDate;					// first loaded day in the views (always set to the first day of the year)
@property (nonatomic) NSDate *currentDate;
@property (nonatomic) NSDate *maxStartDate;						// maximum date for the start of a loaded page of the collection view - set with dateRange, nil for infinite scrolling
@property (nonatomic) NSDateFormatter *dateFormatter;			// used to format month and year headers

@end


@implementation MGCYearCalendarView

// readonly properties whose getter's defined are not auto-synthesized
@synthesize eventsView = _eventsView;
@synthesize verticalScrollView = _verticalScrollView;

#pragma mark - Initialization

- (void)setup
{
    _calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    comps.year = -kYearsLoadingStep;
    NSDate *firstInThisYear = [_calendar mgc_startOfYearForDate:[NSDate date]];
    _startDate = [self.calendar dateByAddingComponents:comps toDate:firstInThisYear options:0];

    _dateFormatter = [NSDateFormatter new];
    _dateFormatter.calendar = _calendar;
    if (isiPad) {
        //NSLog(@"---------------- iPAD ------------------");
        _daysFont = [UIFont systemFontOfSize:kDefaultDayFontSize];
        _headerFont = [UIFont boldSystemFontOfSize:kDefaultMonthHeaderFontSize];
    }
    else if(IS_IPHONE_6_PLUS){
        _daysFont = [UIFont systemFontOfSize:kDefaultDayFontSizeiPhone];
        _headerFont = [UIFont boldSystemFontOfSize:kDefaultMonthHeaderFontSizeiPhone];
    } else if(IS_IPHONE_6){
        _daysFont = [UIFont systemFontOfSize:(kDefaultDayFontSizeiPhone - 1)];
        _headerFont = [UIFont boldSystemFontOfSize:(kDefaultMonthHeaderFontSizeiPhone - 1)];
    } else {
        _daysFont = [UIFont systemFontOfSize:(kDefaultDayFontSizeiPhone - 2)];
        _headerFont = [UIFont boldSystemFontOfSize:(kDefaultMonthHeaderFontSizeiPhone - 2)];
    }
    
    self.backgroundColor = [UIColor clearColor];
}

- (NSDate *)currentDateForCenterDate {
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    comps.year = kYearsLoadingStep;
    return [self.calendar dateByAddingComponents:comps toDate:_startDate options:0];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setup];
    }
    return self;
}

- (void)setCalendar:(NSCalendar *)calendar
{
    _calendar = calendar;
}

#pragma mark - Properties

- (UICollectionViewFlowLayout*)layout
{
    return (UICollectionViewFlowLayout*)self.eventsView.collectionViewLayout;
}

- (void)setDateRange:(MGCDateRange *)dateRange
{
    // nil dateRange means 'inifinite' scrolling
    if (dateRange == nil)
    {
        _dateRange = nil;
        self.maxStartDate = nil;
        return;
    }
    
    // adjust start and end date on year boundaries (always first day of year)
    NSDate *start = [self.calendar mgc_startOfYearForDate:dateRange.start];
    
    NSDateComponents *comps = [NSDateComponents new];
    comps.year = 1;
    NSDate *end = [self.calendar dateByAddingComponents:comps toDate:[self.calendar mgc_startOfYearForDate:dateRange.end] options:0];
    
    _dateRange = [MGCDateRange dateRangeWithStart:start end:end];
    
    // calc max start date
    comps.year = -(2 * kYearsLoadingStep + 1);
    self.maxStartDate = [self.calendar dateByAddingComponents:comps toDate:_dateRange.end options:0];
    if ([self.maxStartDate compare:_dateRange.start] == NSOrderedAscending)
        self.maxStartDate = _dateRange.start;
    
    // adjust startDate if not in new range
    if (![_dateRange containsDate:self.startDate])
        self.startDate = _dateRange.start;

    // reload ?
}

- (MGCDateRange*)visibleMonthsRange
{
    MGCDateRange *range = nil;
    
    NSArray *visible = [[self.eventsView indexPathsForVisibleItems]sortedArrayUsingSelector:@selector(compare:)];
    if (visible.count)
    {
        NSDate *first = [self dateForIndexPath:[visible firstObject]];
        NSDate *last = [self dateForIndexPath:[visible lastObject]];
        
        // end date of the range is excluded, so set it to next month
        NSDateComponents *comps = [NSDateComponents new];
        comps.month = 1;
        last = [self.calendar dateByAddingComponents:comps toDate:last options:0];
        
        range = [MGCDateRange dateRangeWithStart:first end:last];
    }
    
    return range;
}


#pragma mark - Utilities

- (NSDate*)dateForIndexPath:(NSIndexPath*)indexPath
{
    NSDateComponents *comp = [NSDateComponents new];
    comp.year = indexPath.section;
    // In horizontal scrolling mode, lines are laid our vertically and then horizontally, adjust date accordingly
    if (self.layout.scrollDirection == UICollectionViewScrollDirectionHorizontal) {
        UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
        NSUInteger rowCount = [self rowCountForOrientation:orientation];
        NSUInteger colCount = [self colCountForOrientation:orientation];
        NSUInteger oldRow = indexPath.item % rowCount;
        NSUInteger oldCol = indexPath.item / rowCount;
        comp.month = (oldRow * colCount) + oldCol;
    } else {
        comp.month = indexPath.item;
    }
    return [self.calendar dateByAddingComponents:comp toDate:self.startDate options:0];
}

- (NSInteger)numberOfMonthsForYearAtIndex:(NSInteger)year
{
    NSDate *date = [self dateForIndexPath:[NSIndexPath indexPathForItem:0 inSection:year]];
    return [self.calendar rangeOfUnit:NSCalendarUnitMonth inUnit:NSCalendarUnitYear forDate:date].length;
}

- (CGRect)rectForYearAtIndex:(NSUInteger)year
{
    NSIndexPath *first = [NSIndexPath indexPathForItem:0 inSection:year];
    
    CGFloat top = [self.eventsView layoutAttributesForSupplementaryElementOfKind:UICollectionElementKindSectionHeader atIndexPath:first].frame.origin.x;
    
    NSDate *date = [self dateForIndexPath:first];
    NSUInteger lastMonth = [self.calendar rangeOfUnit:NSCalendarUnitMonth inUnit:NSCalendarUnitYear forDate:date].length - 1;
    
    NSIndexPath *last = [NSIndexPath indexPathForItem:lastMonth inSection:year];
    CGFloat bottom = CGRectGetMaxX([self.eventsView layoutAttributesForItemAtIndexPath:last].frame) + self.layout.sectionInset.right;
    return CGRectMake(top, 0, top - bottom, self.bounds.size.height);
}

- (NSAttributedString*)headerTextForMonthAtIndexPath:(NSIndexPath*)indexPath
{
    NSDate *date = [self dateForIndexPath:indexPath];
    
    if ([self.delegate respondsToSelector:@selector(calendarYearView:headerTextForMonthAtDate:)])
    {
        return [self.delegate calendarYearView:self headerTextForMonthAtDate:date];
    }
    else
    {
        self.dateFormatter.dateFormat = @"MMMM";
        NSString *dateStr = [self.dateFormatter stringFromDate:date];
        return [[NSAttributedString alloc]initWithString:dateStr attributes:@{ NSFontAttributeName:self.headerFont }];
    }
}

- (NSDate *)yearFromOffset:(CGFloat)xOffset {
    NSDate *year = self.startDate;
    CGFloat x = 0;
    
    NSInteger pageNum = (int)[self pageNumberForContentOffset:CGPointMake(xOffset, 0.0) orientation:[[UIDevice currentDevice] orientation]];
    xOffset = pageNum * [self pageWidth];
    
    while (x < fabs(xOffset)) {
        year = [self.calendar dateByAddingUnit:NSCalendarUnitYear value:(xOffset > 0 ? 1 : -1) toDate:year options:0];
        x += [self pageWidth];
    };

    return year;
}

- (CGFloat)xOffsetForYear:(NSDate*)date orientation:(UIDeviceOrientation)orientation {
    NSDate *startOfYear =  [self.calendar mgc_startOfYearForDate:date];

    NSDateComponents *comps = [self.calendar components:NSCalendarUnitYear fromDate:self.startDate toDate:startOfYear options:0];
    NSUInteger yearsDiff = labs(comps.year);

    CGFloat offset = 0;

    CGFloat pageWidth = [self pageWidthForOrientation:orientation];
    for (int i = 0; i < yearsDiff; i++) {
        offset += pageWidth;
    }

    if ([startOfYear compare:self.startDate] == NSOrderedAscending) {
        offset = -offset;
    }
    return offset;
}


// returns the offset from startDate to given month
- (CGFloat)xOffsetForYear:(NSDate*)date
{
    return [self xOffsetForYear:date orientation:[[UIDevice currentDevice] orientation]];
}

#pragma mark - Public

- (NSDate*)dateForMonthAtPoint:(CGPoint)pt
{
    pt = [self.eventsView convertPoint:pt fromView:self];
    NSIndexPath *path = [self.eventsView indexPathForItemAtPoint:pt];
    if (path)
    {
        return [self dateForIndexPath:path];
    }
    return nil;
}

-(void)scrollToDate:(NSDate*)date animated:(BOOL)animated
{
    // check if date in range
    if (self.dateRange && ![self.dateRange containsDate:date])
        [NSException raise:@"Invalid parameter" format:@"date %@ is not in range %@ for this calendar view", date, self.dateRange];
    
    NSDate *firstInYear = [self.calendar mgc_startOfYearForDate:date];
    
    CGPoint contentOffset = self.eventsView.contentOffset;
    contentOffset.x = [self xOffsetForYear:firstInYear];
    self.eventsView.contentOffset = contentOffset;
    [self recenterIfNeeded];
    self.currentDate = [self yearFromOffset:_eventsView.contentOffset.x];

    if ([self.delegate respondsToSelector:@selector(calendarYearViewDidScroll:)])
    {
        [self.delegate calendarYearViewDidScroll:self];
    }
}

#pragma mark - Scrolling

// adjusts startDate so that year at given date is centered.
// returns the distance in year between old and new start date
- (NSInteger)adjustStartDateForCenteredYear:(NSDate*)date
{
    NSDate *start = [self.calendar dateByAddingUnit:NSCalendarUnitYear value:-kYearsLoadingStep toDate:date options:0];
    if ([start compare:self.dateRange.start] == NSOrderedAscending) {
        start = self.dateRange.start;
    }
    else if ([start compare:self.maxStartDate] == NSOrderedDescending) {
        start = self.maxStartDate;
    }

    NSInteger diff = (int)[self.calendar components:NSCalendarUnitYear fromDate:self.startDate toDate:start options:0].year;

    self.startDate = start;
    return diff;
}

- (CGFloat)pageWidth {
    return [self pageWidthForOrientation:[[UIDevice currentDevice] orientation]];
}

- (CGFloat)pageWidthForOrientation:(UIDeviceOrientation)orientation {
    NSUInteger colCount = [self colCountForOrientation:orientation];
    return (self.layout.itemSize.width) * colCount + ((colCount -1) * self.layout.minimumLineSpacing) + self.layout.sectionInset.left + self.layout.sectionInset.right;
}

// returns YES if the collection view was reloaded
- (BOOL)recenterIfNeeded
{
    CGFloat xOffset = self.eventsView.contentOffset.x;
    CGFloat contentWidth = self.eventsView.contentSize.width;
    if (xOffset < 0 || CGRectGetMaxX(self.eventsView.bounds) + [self pageWidth] > contentWidth) {

        NSDate *oldStart = [self.startDate copy];

        NSDate *centerYear = [self yearFromOffset:xOffset];//  [self monthFromOffset:xOffset];
        
        NSLog(@"=========== Recenter : %@ ============", centerYear);
        NSInteger yearOffset = [self adjustStartDateForCenteredYear:centerYear];

        if (yearOffset != 0) {
            CGFloat x = [self xOffsetForYear:oldStart];

            CGPoint offset = self.eventsView.contentOffset;
            offset.x = x + xOffset;
            self.eventsView.contentOffset = offset;
            [self.eventsView reloadData];

            return YES;
        }
    }
    return NO;
}

// returns YES if collection views were reloaded
- (BOOL)reloadCollectionViewIfNeeded
{
    
    CGPoint contentOffset = self.eventsView.contentOffset;
    
    if (contentOffset.x < 0.0f || CGRectGetMaxX(self.eventsView.bounds) > self.eventsView.contentSize.width)
    {
        CGPoint offset = self.eventsView.contentOffset;
        
        [self.eventsView setContentOffset:offset];
        [self recenterIfNeeded];
        
        return YES;
    }
    return NO;
}

#pragma mark - Subviews

- (UICollectionView*)eventsView
{
    if (!_eventsView)
    {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        _eventsView = [[YearEventsView alloc]initWithFrame:CGRectNull collectionViewLayout:layout];
        _eventsView.yearView = self;
        _eventsView.backgroundColor = [UIColor whiteColor];
        _eventsView.dataSource = self;
        _eventsView.delegate = self;
        _eventsView.showsHorizontalScrollIndicator = NO;
        _eventsView.showsVerticalScrollIndicator = NO;
        _eventsView.scrollsToTop = NO;
        _eventsView.pagingEnabled = YES;
        
        if(isiPad) {
            //NSLog(@"---------------- iPAD ------------------");
            _eventsView.contentInset = UIEdgeInsetsMake(0, 60, 0, 60);
        }
        else{
            _eventsView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }

        [_eventsView registerClass:MGCYearCalendarMonthCell.class forCellWithReuseIdentifier:MonthCellReuseIdentifier];
        [_eventsView registerClass:MGCYearCalendarMonthHeaderView.class forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:YearHeaderReuseIdentifier];
    }
    return _eventsView;
}

- (UIScrollView *)verticalScrollView {
    if (!_verticalScrollView) {
        _verticalScrollView = [[UIScrollView alloc] initWithFrame:CGRectNull];
        self.verticalScrollView.showsVerticalScrollIndicator = NO;
        self.verticalScrollView.delaysContentTouches = NO;
        _verticalScrollView.bounces = NO;
        _verticalScrollView.backgroundColor = [UIColor whiteColor];
    }
    return _verticalScrollView;
}

- (NSUInteger)rowCountForOrientation:(UIDeviceOrientation) orientation {
    if (UIDeviceOrientationIsLandscape(orientation)) {
        return 3;
    } else {
        return 4;
    }
}

- (NSUInteger)colCountForOrientation:(UIDeviceOrientation) orientation {
    if (UIDeviceOrientationIsLandscape(orientation)) {
        return 4;
    } else {
        return 3;
    }
}

#pragma mark - UIView

- (void)reloadScrollPositionOldOrientation:(UIDeviceOrientation)oldOrientation {

    UIDeviceOrientation toOrientation = [[UIDevice currentDevice] orientation];
    BOOL newIsLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    BOOL oldIsLandscape = UIDeviceOrientationIsLandscape(oldOrientation);
    if (oldIsLandscape == newIsLandscape) { return; }

    _verticalScrollView.contentOffset = CGPointZero;
    _verticalScrollView.contentInset = UIEdgeInsetsZero;
    
    CGPoint contentOffset = self.eventsView.contentOffset;
    contentOffset.x = [self xOffsetForYear:self.currentDate orientation:toOrientation];
    [self recenterIfNeeded];
    _eventsView.contentOffset = contentOffset;
}

- (void)layoutSubviews
{
    if (!self.verticalScrollView.superview) {
        [self addSubview:self.verticalScrollView];
    }
    
    if (!self.eventsView.superview)
    {
        [self.verticalScrollView addSubview:self.eventsView];
        [self.eventsView reloadData];
        self.eventsView.delaysContentTouches = YES;
    }
    MGCMonthMiniCalendarView *cal = [MGCMonthMiniCalendarView new];
    cal.calendar = self.calendar;
    cal.daysFont = self.daysFont;
    cal.headerText = [self headerTextForMonthAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    CGSize cellSize =  [cal preferredSizeYearWise:YES];
    
    if (isiPad) {
        //NSLog(@"---------------- iPAD ------------------");
        self.layout.sectionInset = UIEdgeInsetsMake(30, 60, 30, 60);
        self.layout.minimumInteritemSpacing = kCellMinimumSpacing;
        self.layout.minimumLineSpacing = kCellMinimumSpacing;
    }
    else{
        //NSLog(@"---------------- iPhone ------------------");
        self.layout.sectionInset = UIEdgeInsetsMake(20, 5, 20, 5);
        self.layout.minimumInteritemSpacing = kCellMinimumSpacingiPhone;
        self.layout.minimumLineSpacing = kCellMinimumSpacingiPhone;
    }
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    NSUInteger rowCount = [self rowCountForOrientation:orientation];
    NSUInteger colCount = [self colCountForOrientation:orientation];
    
    CGFloat proposedYearHeight = cellSize.height * rowCount + (self.layout.minimumInteritemSpacing * (rowCount - 1)) + self.layout.sectionInset.top + self.layout.sectionInset.bottom;
    CGFloat yearHeight = proposedYearHeight;
    
    _verticalScrollView.frame = self.bounds;
    CGRect eventsFrame = self.verticalScrollView.bounds;
    eventsFrame.size.height = yearHeight;
    self.eventsView.frame = eventsFrame;

    // Adjust insets to allow just enough space to lay out desired number of cells in available width
    CGFloat availableWidth = self.eventsView.frame.size.width - self.layout.sectionInset.right - self.layout.sectionInset.left;
    CGFloat extraWidth = availableWidth - (colCount * cellSize.width);
    CGFloat itemSpacing = extraWidth / (colCount - 1);
        self.layout.minimumLineSpacing = itemSpacing;
    
    self.layout.itemSize = cellSize;
    
    self.layout.headerReferenceSize = CGSizeMake(self.bounds.size.width, 60);
    if ([self.delegate respondsToSelector:@selector(heightForYearHeaderInCalendarYearView:)])
    {
        CGFloat height = [self.delegate heightForYearHeaderInCalendarYearView:self];
        self.layout.headerReferenceSize = CGSizeMake(0, height);
    }

    self.verticalScrollView.frame = self.bounds;

    CGSize yearPageContentSize = [self.layout collectionViewContentSize];
    yearPageContentSize.width = self.bounds.size.width;
    self.verticalScrollView.contentSize = yearPageContentSize;
    [self.eventsView.collectionViewLayout invalidateLayout];
    [self.eventsView reloadData];
    
    if(self.currentDate == nil) {
        self.currentDate = [NSDate date];
    }
    self.eventsView.contentOffset = CGPointMake([self xOffsetForYear:self.currentDate orientation:orientation], 0);
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView
{
    NSInteger numSections = 2 * kYearsLoadingStep + 1;
    if (self.dateRange)
    {
        NSInteger diff = [self.calendar components:NSCalendarUnitYear fromDate:self.startDate toDate:self.dateRange.end options:0].year;
        if (diff < 3 * kYearsLoadingStep + 1)
            numSections = diff;
    }
    return numSections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self numberOfMonthsForYearAtIndex:section];
}

- (UICollectionViewCell*)collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(NSIndexPath*)indexPath
{
    NSDate *date = [self dateForIndexPath:indexPath];
    
    MGCYearCalendarMonthCell* cell = [self.eventsView dequeueReusableCellWithReuseIdentifier:MonthCellReuseIdentifier forIndexPath:indexPath];
    
    cell.calendarView.calendar = self.calendar;
    cell.calendarView.date = date;
    cell.calendarView.daysFont = self.daysFont;
    cell.calendarView.delegate = self;
    cell.calendarView.headerText = [self headerTextForMonthAtIndexPath:indexPath];
    
    
    cell.calendarView.highlightedDays = nil;
    if ([self.calendar mgc_isDate:date sameMonthAsDate:[NSDate date]])
    {
        NSUInteger i = [self.calendar components:NSCalendarUnitDay fromDate:date toDate:[NSDate date] options:0].day + 1;
        cell.calendarView.highlightedDays = [NSIndexSet indexSetWithIndex:i];
        cell.calendarView.highlightColor = APP_COLOR;
        cell.calendarView.backgroundColor = [UIColor colorWithRed:0.785 green:0.937 blue:0.921 alpha:0.6];
        cell.calendarView.layer.borderWidth = 1.f;
        cell.calendarView.layer.borderColor = (APP_COLOR).CGColor;
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithAttributedString:cell.calendarView.headerText];
        [str addAttribute:NSForegroundColorAttributeName value:APP_COLOR range:NSMakeRange(0, str.length)];
        cell.calendarView.headerText = str;
    } else {
        cell.calendarView.highlightColor = APP_COLOR;
        cell.calendarView.backgroundColor = [UIColor clearColor];
        cell.calendarView.layer.borderWidth = 0.f;
        cell.calendarView.layer.borderColor = [UIColor clearColor].CGColor;
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithAttributedString:cell.calendarView.headerText];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, str.length)];
        cell.calendarView.headerText = str;
    }
    
    [cell.calendarView setNeedsDisplay];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    reusableview.frame = CGRectZero;
    
    if (kind == UICollectionElementKindSectionHeader)
    {
        MGCYearCalendarMonthHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:YearHeaderReuseIdentifier forIndexPath:indexPath];
        
        NSDate *date = [self dateForIndexPath:indexPath];
        if ([self.delegate respondsToSelector:@selector(calendarYearView:headerTextForYearAtDate:)])
        {
            NSAttributedString *str = [self.delegate calendarYearView:self headerTextForYearAtDate:date];
            headerView.label.attributedText = str;
        }
        else
        {
            self.dateFormatter.dateFormat = @"yyyy";
            NSString *str = [self.dateFormatter stringFromDate:date];
            if (isiPad) {
                //NSLog(@"---------------- iPAD ------------------");
                headerView.label.font = [UIFont systemFontOfSize:kDefaultYearHeaderFontSize];
            }
            else if(IS_IPHONE_6_PLUS){
                headerView.label.font = [UIFont systemFontOfSize:kDefaultYearHeaderFontSizeiPhone];
            } else if(IS_IPHONE_6) {
                headerView.label.font = [UIFont systemFontOfSize:(kDefaultYearHeaderFontSizeiPhone - 1)];
            } else {
                headerView.label.font = [UIFont systemFontOfSize:(kDefaultYearHeaderFontSizeiPhone - 2)];
            }
            headerView.label.text = str;
        }
        
        [headerView setNeedsDisplay];
        reusableview = headerView;
    }
    
    return reusableview;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (NSUInteger)pageNumberForContentOffset:(CGPoint)contentOffset orientation:(UIDeviceOrientation)orientation {
    CGFloat pageWidth = [self pageWidthForOrientation:orientation];
    CGFloat pageNumber = contentOffset.x / pageWidth;
    CGFloat remain = (pageNumber - floorf(pageNumber));
    if (remain < 0.5) {
        pageNumber = floorf(pageNumber);
    } else {
        pageNumber = ceilf(pageNumber);
    }
    return (NSUInteger)pageNumber;
}

// TODO(CRD): Reduce redudancy with scrollViewWillEndDragging:
- (void)snapEventsViewToNearestPage {
    NSUInteger pageNumber = [self pageNumberForContentOffset:self.eventsView.contentOffset orientation:[[UIDevice currentDevice] orientation]];
    CGPoint newOffset = _eventsView.contentOffset;
    newOffset.x = pageNumber * [self pageWidth];
    _eventsView.contentOffset = newOffset;
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    float pageWidth = [self pageWidth];

    float currentOffset = scrollView.contentOffset.x;
    float targetOffset = targetContentOffset->x;
    float newTargetOffset = 0;
    
    BOOL incrDate = targetOffset > currentOffset;
    if (incrDate)
        newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
    else
        newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
    
    if (newTargetOffset < 0)
        newTargetOffset = 0;
    else if (newTargetOffset > scrollView.contentSize.width)
        newTargetOffset = scrollView.contentSize.width;
    
    targetContentOffset->x = newTargetOffset;
    [scrollView setContentOffset:CGPointMake(newTargetOffset, 0) animated:YES];
    self.currentDate = [self yearFromOffset:newTargetOffset];
}

- (void)scrollViewDidScroll:(UIScrollView*)scrollview
{
    [self recenterIfNeeded];
    if ([self.delegate respondsToSelector:@selector(calendarYearViewDidScroll:)])
    {
        [self.delegate calendarYearViewDidScroll:self];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(calendarYearView:didSelectMonthAtDate:)])
    {
        NSDate *date = [self dateForIndexPath:indexPath];
        [self.delegate calendarYearView:self didSelectMonthAtDate:date];
    }
}

#pragma mark - MonthCalendarDelegate

//- (UIColor*)monthCalendar:(MonthCalendar*)calendar backgroundColorForDayAtIndex:(NSUInteger)index
//{
//	return [UIColor yellowColor];
//}

@end
