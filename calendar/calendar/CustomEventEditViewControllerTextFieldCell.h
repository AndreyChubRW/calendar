//
//  CustomEventEditViewControllerLabelCell.h
//  calendar
//
//  Created by Andrey on 9/16/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellModel.h"

@interface CustomEventEditViewControllerTextFieldCell : UITableViewCell

@property (copy, nonatomic) void(^actionTextViewBlock)(NSString*);
@property (copy, nonatomic) void(^actionBtnBlock)();

- (void)setCellData:(TableViewCellModel *)dict;

@end
