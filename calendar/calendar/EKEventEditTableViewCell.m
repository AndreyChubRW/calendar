//
//  EKEventEditTableViewCell.m
//  calendar
//
//  Created by Andrey Chub on 9/15/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "EKEventEditTableViewCell.h"

@interface EKEventEditTableViewCell ()

@property (strong, nonatomic) UIImageView *cellImageView;

@property (strong, nonatomic) UITextField *cellTextField;

@property (strong, nonatomic) UILabel *cellLabel;

// second section
@property (strong, nonatomic) UISwitch *cellSwitch;
@property (strong, nonatomic) UILabel *detailLabel;
@property (strong, nonatomic) UIImageView *cellDetailImageView;

@end

@implementation EKEventEditTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.cellImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 43, 55)];
        [self addSubview:self.cellImageView];
        
        self.cellTextField = [[UITextField alloc] initWithFrame:CGRectZero];
        [self addSubview:self.cellTextField];
        NSAttributedString *placeholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:13]}];
        [self.cellTextField setAttributedPlaceholder:placeholder];
        [self.cellTextField setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
        [self.cellTextField setTextColor:[UIColor darkGrayColor]];
        [self.cellTextField setTranslatesAutoresizingMaskIntoConstraints:NO];
        [[self.cellTextField.leadingAnchor constraintEqualToAnchor:self.cellImageView.trailingAnchor constant:4] setActive:YES];
        [[self.cellTextField.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-3] setActive:YES];
        [[self.cellTextField.heightAnchor constraintEqualToConstant:30] setActive:YES];
        [[self.cellTextField.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor] setActive:YES];
        
        self.cellLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self addSubview:self.cellLabel];
        [self.cellLabel setTextColor:[UIColor darkGrayColor]];
        [self.cellLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
        [self.cellLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [[self.cellLabel.leadingAnchor constraintEqualToAnchor:self.cellImageView.trailingAnchor constant:4] setActive:YES];
        [[self.cellLabel.widthAnchor constraintEqualToConstant:70] setActive:YES];
        [[self.cellLabel.heightAnchor constraintEqualToConstant:30] setActive:YES];
        [[self.cellLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:14] setActive:YES];
        
        self.cellSwitch = [[UISwitch alloc] init];
        [self addSubview:self.cellSwitch];
        [self.cellSwitch addTarget:self action:@selector(cellSwitchAction) forControlEvents:UIControlEventValueChanged];
        [self.cellSwitch setTranslatesAutoresizingMaskIntoConstraints:NO];
        [[self.cellSwitch.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor] setActive:YES];
        [[self.cellSwitch.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor constant:-16] setActive:YES];
        self.cellSwitch.opaque = YES;
        self.cellSwitch.clearsContextBeforeDrawing = YES;
        self.cellSwitch.onTintColor = [UIColor colorWithRed:(CGFloat)(44.f/255.f) green:(CGFloat)(186.f/255.f) blue:(CGFloat)(165.f/255.f) alpha:1];
        
        self.cellDetailImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self addSubview:self.cellDetailImageView];
        [self.cellDetailImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
        self.cellDetailImageView.image = [UIImage imageNamed:@"detailImage"];
        [[self.cellDetailImageView.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor] setActive:YES];
        [[self.cellDetailImageView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-16] setActive:YES];
        
        self.detailLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self addSubview:self.detailLabel];
        [self.detailLabel setTextColor:[UIColor darkGrayColor]];
        [self.detailLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
        self.detailLabel.textAlignment = NSTextAlignmentRight;
        [self.detailLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [[self.detailLabel.trailingAnchor constraintEqualToAnchor:self.cellDetailImageView.leadingAnchor constant:-4] setActive:YES];
        [[self.detailLabel.heightAnchor constraintEqualToConstant:30] setActive:YES];
        [[self.detailLabel.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor] setActive:YES];
        
        UIView *separator = [[UIView alloc] initWithFrame:CGRectZero];
        separator.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:separator];
        [separator setTranslatesAutoresizingMaskIntoConstraints:NO];
        [[separator.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor] setActive:YES];
        [[separator.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor] setActive:YES];
        [[separator.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor] setActive:YES];
        [[separator.heightAnchor constraintEqualToConstant:0.5] setActive:YES];
    }
    return self;
 }

- (void)setCellDict:(NSDictionary *)cellDict {
    _cellDict = cellDict;
    
    self.cellImageView.image = [UIImage imageNamed:cellDict[@"image"]];
}

- (void)setIsFirstSection:(BOOL)isFirstSection {
    _isFirstSection = isFirstSection;
    if (isFirstSection == YES) {
        self.cellLabel.hidden = YES;
        self.cellTextField.hidden = NO;
        self.cellTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.cellDict[@"placeholder"] attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:13]}];
    }
    else {
        self.cellTextField.hidden = YES;
        self.cellLabel.hidden = NO;
        self.cellLabel.text = self.cellDict[@"placeholder"];
    }
}

- (void)setIsThisFirstElementOfTheSecondSection:(BOOL)isThisFirstElementOfTheSecondSection {
    _isThisFirstElementOfTheSecondSection = isThisFirstElementOfTheSecondSection;
    
    
    self.cellSwitch.hidden = isThisFirstElementOfTheSecondSection ? NO : YES;
}

- (void)setIsPickerCell:(BOOL)isPickerCell {
    _isPickerCell = isPickerCell;
    
    self.detailLabel.text = self.cellDict[@"text"];
    self.detailLabel.hidden = isPickerCell ? NO : YES;
}

-(void)setIsCellWithPushArrow:(BOOL)isCellWithPushArrow {
    _isCellWithPushArrow = isCellWithPushArrow;
    
    self.cellDetailImageView.hidden = isCellWithPushArrow ? NO : YES;
}

#pragma mark - Actions 

- (void)cellSwitchAction {
    
}

@end
