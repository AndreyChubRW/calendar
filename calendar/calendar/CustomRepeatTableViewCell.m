//
//  CustomRepeatTableViewCell.m
//  calendar
//
//  Created by Andrey on 9/21/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "CustomRepeatTableViewCell.h"

@interface CustomRepeatTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;

@end

@implementation CustomRepeatTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCellDataWithDict:(RepeatCellModel *)dict {
    
    self.titleTextLabel.text = dict.title;
    
    if (dict.isSelected) {
        
        self.pictureImageView.image = [UIImage imageNamed:@"selectedColorCheckMark"];
    }
    else {
        
        self.pictureImageView.image = [UIImage imageNamed:@""];
    }
}


@end
