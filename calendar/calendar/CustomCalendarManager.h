//
//  CustomCalendarManager.h
//  calendar
//
//  Created by Andrey on 9/22/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomCalendarModell.h"

@interface CustomCalendarManager : NSObject

@property (strong, nonatomic) MGCEventKitSupport *eventKitSupport;

+ (instancetype)sharedInstance;

- (void)createColuorCalendars;
- (void)deleteCustomCalendars;
- (EKCalendar *)getCalendarAtIndex:(NSUInteger)index;

@end
