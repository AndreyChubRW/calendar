//
//  WeekViewController.swift
//  calendar
//
//  Created by forever on 8/23/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

import UIKit
//import CalendarLib


protocol WeekViewControllerDelegate : MGCDayPlannerEKViewControllerDelegate, CalendarViewControllerDelegate, UIViewControllerTransitioningDelegate
{
    
}

extension NSDate
{
    convenience
    init(dateString:String) {
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "el_GR")
        let d = dateStringFormatter.dateFromString(dateString)!
        self.init(timeInterval:0, sinceDate:d)
    }
}



class WeekViewController: MGCDayPlannerEKViewController, CalendarViewControllerNavigation
{
    var myDelegate:WeekViewControllerDelegate?
    var centerDate :NSDate?
        {
        get{
            let date = self.dayPlannerView.dateAtPoint(self.dayPlannerView.center, rounded:false)
            return date
        }
    }
    
    var dateFormatter: NSDateFormatter?   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dayPlannerView.backgroundColor = UIColor.clearColor()
        self.dayPlannerView.backgroundView = UIView()
        self.dayPlannerView.backgroundView.backgroundColor = UIColor.whiteColor()
        self.dayPlannerView.dateFormat = "eee\nd MMM"
        self.dayPlannerView.dayHeaderHeight = 60
        self.dayPlannerView.canCreateEvents = true
        self.dayPlannerView.canMoveEvents = true
        
    }
    
    override func dayPlannerView(view: MGCDayPlannerView!, canCreateNewEventOfType type: MGCEventType, atDate date: NSDate!) -> Bool {
        let comps = self.calendar!.components(.Weekday, fromDate: date!)
        return comps.weekday != 1
    }
    
    override func dayPlannerView(view: MGCDayPlannerView!, didScroll scrollType: MGCDayPlannerScrollType)
    {
        let date = view.dateAtPoint(view.center, rounded:true)
        if (date != nil && (self as? CalendarViewController) != nil)
        {
            (self.delegate! as! CalendarViewControllerDelegate).calendarViewController(self, didShowDate: date!)
        }
    }
    
    override func dayPlannerView(view: MGCDayPlannerView!, canMoveEventOfType type: MGCEventType, atIndex index: UInt, date: NSDate!, toType targetType: MGCEventType, date targetDate: NSDate!) -> Bool {
        let comps = self.calendar!.components(.Weekday, fromDate: targetDate)
        return (comps.weekday != 1 && comps.weekday != 7)
    }
    
    override func dayPlannerView(view: MGCDayPlannerView!, attributedStringForDayHeaderAtDate date: NSDate!) -> NSAttributedString! {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "EEEEE\nd";
        let dayStr = dateFormatter.stringFromDate(date)
//        dayStr = dayStr.substringToIndex(dayStr.startIndex.advancedBy(1))
        
        let font = UIFont.systemFontOfSize(13)
        
        let attrStr = NSMutableAttributedString(string: dayStr, attributes: [ NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
        let dayNamefont = UIFont.systemFontOfSize(15)
        let dayNumberStart = dayStr.characters.indexOf("\n")?.advancedBy(1)
        let start = dayStr.startIndex.distanceTo(dayNumberStart!)
        let length = dayNumberStart!.distanceTo(dayStr.endIndex)
        let range = NSMakeRange(start, length)
        
        attrStr.addAttributes([NSFontAttributeName: dayNamefont, NSForegroundColorAttributeName: UIColor.blackColor()], range: range)
        
        let now = NSDate()
        if view.calendar!.mgc_isDate(date, sameDayAsDate: now)
        {
            let boldFont = UIFont.systemFontOfSize(15)
            
            let mark = MGCCircleMark()
            mark.yOffset = boldFont.descender - mark.margin;
            
            let circleStart = dayStr.characters.indexOf("\n")?.advancedBy(1)
            let start = dayStr.startIndex.distanceTo(circleStart!)
            let length = circleStart!.distanceTo(dayStr.endIndex)
            let range = NSMakeRange(start, length)
            attrStr.addAttributes([NSFontAttributeName: boldFont, NSForegroundColorAttributeName: UIColor.whiteColor(), MGCCircleMarkAttributeName: mark], range: range)
            
            attrStr.processCircleMarksInRange(NSMakeRange(0, attrStr.length))
        }
        
        let para = NSMutableParagraphStyle()
        para.alignment = NSTextAlignment.Center
        attrStr.addAttributes([NSParagraphStyleAttributeName: para], range: NSMakeRange(0, attrStr.length))
        return attrStr
        
    }
    
    func moveToNextPageAnimated(animated: Bool) {
        var date:NSDate? = nil
        self.dayPlannerView.pageForwardAnimated(animated, date: &date)
    }
    
    func moveToPreviousPageAnimated(animated: Bool) {
        var date:NSDate? = nil
        self.dayPlannerView.pageBackwardsAnimated(animated, date: &date)
    }
    
    func moveToDate(date: NSDate, animated: Bool) {
        
        if (self.dayPlannerView!.dateRange == nil || self.dayPlannerView!.dateRange.containsDate(date))
        {
            self.dayPlannerView!.scrollToDate(date, options: MGCDayPlannerScrollType.DateTime, animated:animated)
        }
    }   
    
}
