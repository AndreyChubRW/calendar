//
//  LocationTableViewCell.h
//  calendar
//
//  Created by Andrey on 9/21/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface LocationTableViewCell : UITableViewCell

- (void)setCellData:(MKMapItem *)mkItem;

@end
