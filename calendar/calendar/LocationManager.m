//
//  LocationManager.m
//  TweatScore
//
//  Created by Andrey on 9/13/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import "LocationManager.h"

@interface LocationManager ()

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (copy, nonatomic) CLLocation *userLocation;

@end

@implementation LocationManager

static LocationManager *sharedInstance = nil;

+ (instancetype)sharedInstance {

    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LocationManager alloc] init];
    });
    return sharedInstance;
}

+ (void)startTrackLocation {
    
    [LocationManager sharedInstance];
    sharedInstance.locationManager = [[CLLocationManager alloc] init];
    sharedInstance.locationManager.delegate = sharedInstance;
    [sharedInstance.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [sharedInstance.locationManager requestWhenInUseAuthorization];
    [sharedInstance.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    self.userLocation = locations[0];
}

+ (CLLocation *)getCurrentUserLocation {
    
    return sharedInstance.userLocation;
}

+ (CLLocationCoordinate2D) getLocationFromAddressString:(NSString*) addressStr {
    
    double latitude = 0, longitude = 0;
    NSString *esc_addr = [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}

@end
