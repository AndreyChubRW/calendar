//
//  CustomEKEventEditViewController.m
//  calendar
//
//  Created by Andrey Chub on 9/15/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "CustomEKEventEditViewController.h"
#import "EKEventEditTableViewCell.h"

@interface CustomEKEventEditViewController() <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *mainTableView;
@property (strong, nonatomic) NSMutableArray * dataArray;
@property (strong, nonatomic) UIView *contentEditView;

@property (strong, nonatomic) EKEvent *myEvent;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;


@end

@implementation CustomEKEventEditViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"MMM dd,YYYY   hh:mm:aa"];
    
    self.contentEditView = [[UIView alloc] initWithFrame:self.view.frame];
    self.contentEditView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.contentEditView];
   
    UINavigationBar *bar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 72)];
    bar.barStyle = UIBarStyleBlack;
    bar.opaque = YES;
    bar.translucent = YES;
    bar.clearsContextBeforeDrawing = YES;
    bar.barTintColor = [UIColor colorWithRed:(CGFloat)(44.f/255.f) green:(CGFloat)(186.f/255.f) blue:(CGFloat)(165.f/255.f) alpha:1];
    
    [bar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                  NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:17]}];
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@"ADD EVENT"];
    [bar setItems:@[navItem]];
 
    UIBarButtonItem* cancelBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelEditing)];
    [cancelBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:15]} forState:UIControlStateNormal];
    navItem.leftBarButtonItem = cancelBarButtonItem;
    
    UIBarButtonItem* addBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStyleDone target:self action:@selector(saveEditing)];
    [addBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:15]} forState:UIControlStateNormal];
    
    navItem.rightBarButtonItem = addBarButtonItem;
    [self.view addSubview:bar];
    
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    [self.view addSubview:self.mainTableView];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainTableView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [[self.mainTableView.topAnchor constraintEqualToAnchor:bar.bottomAnchor] setActive:YES];
    [[self.mainTableView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor] setActive:YES];
    [[self.mainTableView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor] setActive:YES];
    [[self.mainTableView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor] setActive:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSMutableArray *firstSectionArray = [[NSMutableArray alloc] initWithObjects:@{
                                                                                  @"image": @"titleImage",
                                                                                  @"placeholder": @"Title",
                                                                                  @"text": @""
                                                                                  },
                                         @{
                                           @"image": @"locationImage",
                                           @"placeholder": @"Location",
                                           @"text": @""
                                           }, nil];
    
    NSMutableArray *secondSectionArray = [[NSMutableArray alloc] initWithObjects:@{
                                                                                   @"image": @"allDayImage",
                                                                                   @"placeholder": @"All Day",
                                                                                   @"switch": @(1)
                                                                                   },
                                          @{
                                            @"image": @"startsImage",
                                            @"placeholder": @"Starts",
                                            @"text": [self.dateFormatter stringFromDate:self.event.startDate]
                                            },
                                          
                                          
                                          @{
                                            @"image": @"endsImage",
                                            @"placeholder": @"Ends",
                                            @"text": [self.dateFormatter stringFromDate:self.event.endDate]
                                            },
                                          @{
                                            @"image": @"repeatImage",
                                            @"placeholder": @"Repeat",
                                            @"text": @""
                                            },
                                          @{
                                            @"image": @"endRepeatImage",
                                            @"placeholder": @"End Repeat",
                                            @"text": @""
                                            },
                                          @{
                                            @"image": @"colourImage",
                                            @"placeholder": @"Colours",
                                            @"text": @""
                                            }, nil];
    
    self.dataArray = [[NSMutableArray alloc] initWithObjects:firstSectionArray,secondSectionArray, nil];
    
    NSLog(@"%@", self.eventStore);
    NSLog(@"%@", self.event);
    
    [self.mainTableView reloadData];
}

#pragma mark - SETTERS

- (void)setEvent:(EKEvent *)event {
    [super setEvent:event];
}

-(void)setEventStore:(EKEventStore *)eventStore {
    [super setEventStore:eventStore];
}

#pragma mark - View controller preferences

- (void)setupMainTableView {
    
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIViewController *vc = [[UIViewController alloc] init];
    [super pushViewController:vc animated:YES];
//    [self pushViewController:vc animated:YES];
    self.viewControllers = @[vc];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    view.backgroundColor = [UIColor colorWithRed:246.f/255.f green:246.f/255.f blue:246.f/255.f alpha:1];
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, view.frame.size.height - 0.5, view.frame.size.width, 0.5)];
    separatorView.backgroundColor = [UIColor colorWithRed:236.f/255.f green:236.f/255.f blue:236.f/255.f alpha:1];
    [view addSubview:separatorView];
}
- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    view.frame = CGRectMake(0, 0, self.view.frame.size.width, 1);
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 35;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return ((NSArray *)self.dataArray[section]).count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EKEventEditTableViewCell *cell;
    if (!cell) {
        cell = [[EKEventEditTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"identifier"];
    }
    cell.cellDict = (self.dataArray[indexPath.section])[indexPath.row];
    
    cell.isFirstSection = indexPath.section == 0 ? YES : NO;
    NSIndexPath *switchIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    cell.isThisFirstElementOfTheSecondSection = indexPath == switchIndexPath ? YES : NO;
    cell.isPickerCell = indexPath.section == 1 & indexPath.row == 1 || indexPath.section == 1 & indexPath.row == 2 ? YES : NO;
    cell.isCellWithPushArrow = indexPath.section == 1 && indexPath.row > 0 ? YES : NO;
    return cell;
}


#pragma mark - Actions 

- (void)cancelEditing {
    [self.editViewDelegate eventEditViewController:self didCompleteWithAction:EKEventEditViewActionCanceled];
}

- (void)saveEditing {
    [self.editViewDelegate eventEditViewController:self didCompleteWithAction:EKEventEditViewActionSaved];
}



@end
