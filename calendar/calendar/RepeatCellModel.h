//
//  RepeatCellModel.h
//  calendar
//
//  Created by Andrey on 9/21/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RepeatCellModel : NSObject

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSDate *date;
@property (assign, nonatomic) BOOL isSelected;

- (instancetype)initWithTitle:(NSString *)title date:(NSDate *)date state:(BOOL)isSelected;

@end
