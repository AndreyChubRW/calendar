//
//  CustomEKEventEditViewController.h
//  calendar
//
//  Created by Andrey Chub on 9/15/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <EventKitUI/EventKitUI.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUIDefines.h>

@interface CustomEKEventEditViewController : EKEventEditViewController

@end
