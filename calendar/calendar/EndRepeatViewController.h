//
//  EndRepeatViewController.h
//  calendar
//
//  Created by Andrey on 9/22/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import "CustomEventEditViewController.h"
#import "TableViewCellModel.h"

@interface EndRepeatViewController : UIViewController

@property (strong, nonatomic) TableViewCellModel *endRepeatCell;
@property (strong, nonatomic) TableViewCellModel *repeatCell;
@property (weak, nonatomic) id <ActionProtocol> actionDelegate;

- (instancetype)initEndRepeatViewControllerWithEvent:(EKEvent *)event;

@end
