//
//  CustomRepeatViewController.m
//  calendar
//
//  Created by Andrey on 9/21/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "CustomRepeatViewController.h"
#import "CustomRepeatTableViewCell.h"
#import "RepeatCellModel.h"

@interface CustomRepeatViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *tableData;
@property (strong, nonatomic) EKEvent *event;

@end

@implementation CustomRepeatViewController

- (instancetype)initCustomRepeatViewControllerWithEvent:(EKEvent *)event {
    
    self = [[NSBundle mainBundle] loadNibNamed:@"CustomRepeatViewController" owner:self options:nil][0];
    
    if (self) {
        
        self.event = event;
        [self setCellModel];
    }
    
    return self;
}

- (void)setCellModel {
    RepeatCellModel *neverCellModel = [[RepeatCellModel alloc] initWithTitle:@"Never" date:nil state:NO];
    RepeatCellModel *everyDayCellModel = [[RepeatCellModel alloc] initWithTitle:@"Every Day" date:nil state:NO];
    RepeatCellModel *everyWeekCellModel = [[RepeatCellModel alloc] initWithTitle:@"Every Week" date:nil state:NO];
    RepeatCellModel *everyTwoWeeksCellModel = [[RepeatCellModel alloc] initWithTitle:@"Every 2 Weeks" date:nil state:NO];
    RepeatCellModel *everyMonthCellModel = [[RepeatCellModel alloc] initWithTitle:@"Every Month" date:nil state:NO];
    RepeatCellModel *everyYearCellModel = [[RepeatCellModel alloc] initWithTitle:@"Every Year" date:nil state:NO];
    
    self.tableData = @[neverCellModel, everyDayCellModel, everyWeekCellModel, everyTwoWeeksCellModel, everyMonthCellModel, everyYearCellModel];
    
    if (self.event.recurrenceRules.count > 0) {
        
        EKRecurrenceRule *recurrence = [self.event.recurrenceRules firstObject];
        
        if (recurrence.frequency == EKRecurrenceFrequencyDaily) {
            everyDayCellModel.isSelected = YES;
        }
        else if (recurrence.frequency == EKRecurrenceFrequencyWeekly) {
            
            if (recurrence.interval == 1) {
                everyWeekCellModel.isSelected = YES;
            }
            else {
                everyTwoWeeksCellModel.isSelected = YES;
            }
        }
        else if (recurrence.frequency == EKRecurrenceFrequencyMonthly) {
            everyMonthCellModel.isSelected = YES;
        }
        else {
            everyYearCellModel.isSelected = YES;
        }
    } else {
        neverCellModel.isSelected = YES;
    }
    
}

- (IBAction)cancelBtnPressed:(id)sender {
    
    if ([self.actionDelegate respondsToSelector:@selector(dismissViewController)]) {
        
        [self.actionDelegate dismissViewController];
    }
}


#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 32;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.tableData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomRepeatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomRepeatTableViewCell"];
    
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"CustomRepeatTableViewCell" owner:self options:nil][0];
    }
    
    [cell setCellDataWithDict:[self.tableData objectAtIndex:indexPath.row]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    for (RepeatCellModel *model in self.tableData) {
        model.isSelected = NO;
    }
    
    RepeatCellModel *tempModel = [self.tableData objectAtIndex:indexPath.row];
    tempModel.isSelected = YES;
    self.repeatCell.cellText = tempModel.title;
    
    if ([tempModel.title isEqualToString:@"Never"]) {
        
        self.event.recurrenceRules = nil;
    }
    else if ([tempModel.title isEqualToString:@"Every Day"]) {
     
        self.event.recurrenceRules = nil;
        EKRecurrenceRule *recurrence = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyDaily interval:1 end:nil];
        [self.event addRecurrenceRule:recurrence];
    }
    else if ([tempModel.title isEqualToString:@"Every Week"]) {
        
        self.event.recurrenceRules = nil;
        EKRecurrenceRule *recurrence = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyWeekly interval:1 end:nil];
        [self.event addRecurrenceRule:recurrence];
    }
    else if ([tempModel.title isEqualToString:@"Every 2 Weeks"]) {
        
        self.event.recurrenceRules = nil;
        EKRecurrenceRule *recurrence = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyWeekly interval:2 end:nil];
        [self.event addRecurrenceRule:recurrence];
    }
    else if ([tempModel.title isEqualToString:@"Every Month"]) {
        
        self.event.recurrenceRules = nil;
        EKRecurrenceRule *recurrence = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyMonthly interval:1 end:nil];
        [self.event addRecurrenceRule:recurrence];
    }
    else if ([tempModel.title isEqualToString:@"Every Year"]) {
        
        self.event.recurrenceRules = nil;
        EKRecurrenceRule *recurrence = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyYearly interval:1 end:nil];
        [self.event addRecurrenceRule:recurrence];
    }
    else {
        NSLog(@"");
    }
    
    if ([self.actionDelegate respondsToSelector:@selector(dismissViewController)]) {
        
        [self.actionDelegate dismissViewController];
    }
    
    [self.tableView reloadData];
}

@end
