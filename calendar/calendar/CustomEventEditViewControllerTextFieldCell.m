//
//  CustomEventEditViewControllerLabelCell.m
//  calendar
//
//  Created by Andrey on 9/16/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#include "CustomEventEditViewControllerTextFieldCell.h"

@interface CustomEventEditViewControllerTextFieldCell ()

@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *deleteTextBtn;

@end

@implementation CustomEventEditViewControllerTextFieldCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCellData:(TableViewCellModel *)dict {
    
    if ([dict.kind isEqualToString:@"titleCell"]) {
        
        self.textField.userInteractionEnabled = YES;
        self.textField.text = dict.cellText;
    }
    if ([dict.kind isEqualToString:@"locationCell"]) {
        self.textField.userInteractionEnabled = NO;
        self.textField.text = dict.cellText;
    }
    
    self.pictureImageView.image = [UIImage imageNamed:dict.pictureName];
    self.textField.placeholder = dict.placeHolder;
}

- (IBAction)deleteTextBtnPressed:(id)sender {
    
    self.textField.text = @"";
    
    if (self.actionBtnBlock) {
        self.actionBtnBlock();
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField 
{   
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField 
{
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string 
{
    
    if (self.actionTextViewBlock) {
        self.actionTextViewBlock([textField.text stringByReplacingCharactersInRange:range withString:string]);
    }
    
    return YES;
}

@end
