//
//  CustomEventEditViewControllerSwitchCell.h
//  calendar
//
//  Created by Andrey on 9/16/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellModel.h"
#import "DropDownActionDelegate.h"

@interface CustomEventEditViewControllerSwitchCell : UITableViewCell

@property (weak, nonatomic) id <DropDownActionDelegate> actionDelegate;

- (void)setCellData:(TableViewCellModel *)dict;

@end
