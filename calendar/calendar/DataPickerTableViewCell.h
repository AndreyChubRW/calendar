//
//  DataPickerTableViewCell.h
//  calendar
//
//  Created by Andrey on 9/16/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownActionDelegate.h"
#import "TableViewCellModel.h"

@interface DataPickerTableViewCell : UITableViewCell

@property (weak, nonatomic) id <DropDownActionDelegate> actionDelegate;

- (void)setCellData:(TableViewCellModel *)cellModel;

@end
