//
//  CustomEventEditViewControllerSwitchCell.m
//  calendar
//
//  Created by Andrey on 9/16/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "CustomEventEditViewControllerSwitchCell.h"

@interface CustomEventEditViewControllerSwitchCell ()

@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (weak, nonatomic) IBOutlet UISwitch *switcher;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation CustomEventEditViewControllerSwitchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCellData:(TableViewCellModel *)dict {
    
    self.pictureImageView.image = [UIImage imageNamed:dict.pictureName];
    self.titleLabel.text = dict.placeHolder;
}

- (IBAction)switchDidChangeState:(id)sender {
    
    if ([self.actionDelegate respondsToSelector:@selector(changeSwitchStateIsOn:)]) {
        
        if ([self.switcher isOn]) {
            
            [self.actionDelegate changeSwitchStateIsOn:YES];
        }
        else {
            
            [self.actionDelegate changeSwitchStateIsOn:NO];
        }
    }
}

@end
