//
//  CustomEventEditViewController.m
//  calendar
//
//  Created by Andrey on 9/16/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "CustomEventEditViewController.h"
#import "CustomEventEditViewControllerCell.h"
#import "CustomEventEditViewControllerSwitchCell.h"
#import "CustomEventEditViewControllerTextFieldCell.h"
#import "DataPickerTableViewCell.h"
#import "PaletteColorsTableViewCell.h"
#import "TableViewCellModel.h"
#import "CustomRepeatViewController.h"
#import "LocationSearchController.h"
#import "CustomCalendarManager.h"
#import "EndRepeatViewController.h"

@interface CustomEventEditViewController () <UITableViewDataSource, UITableViewDelegate, DropDownActionDelegate, ActionProtocol>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *tableData;
@property (strong, nonatomic) NSArray *firstSectionArray;
@property (strong, nonatomic) NSArray *secondSectionArray;
@property (strong, nonatomic) EKCalendar *testCalendar;

@property (strong, nonatomic) NSString *identif;
@property (strong, nonatomic) TableViewCellModel *stratDropDownCell;
@property (strong, nonatomic) TableViewCellModel *endsDropDownCell;
@property (strong, nonatomic) TableViewCellModel *palletDropDownCell;
@property (strong, nonatomic) TableViewCellModel *repeatCell;
@property (strong, nonatomic) TableViewCellModel *endRepeatCell;
@property (assign, nonatomic) BOOL switcherStateIsChanged;

@end

@implementation CustomEventEditViewController

- (instancetype)initCustomEventEditViewControllerWithEvent:(EKEvent *)event eventKitSupport:(MGCEventKitSupport *)eventKitSupport {
    
    self = [[NSBundle mainBundle] loadNibNamed:@"CustomEventEditViewController" owner:self options:nil][0];
    
    if (self) {
        
        self.event = event;
        self.eventKitSupport = eventKitSupport;
        self.testCalendar = nil;
        [self setTableViewDataModel];
        [self registerNibForTableView];
        
        [CustomCalendarManager sharedInstance].eventKitSupport = eventKitSupport;
        [[CustomCalendarManager sharedInstance] createColuorCalendars];
    }
    
    return self;
}

- (void)registerNibForTableView {
    
    //nsstring from class
    NSArray *reuseIdentifiers = @[@"CustomEventEditViewControllerTextFieldCell", @"CustomEventEditViewControllerSwitchCell", @"CustomEventEditViewControllerCell", @"DataPickerTableViewCell", @"PaletteColorsTableViewCell"];
    
    UINib * nib = [[UINib alloc] init];
    
    for (NSString *cellIdentifier in reuseIdentifiers) {
        nib = [UINib nibWithNibName:cellIdentifier bundle:nil];
        [self.tableView registerNib:nib forCellReuseIdentifier:cellIdentifier];
    }
}

- (void)setTableViewDataModel {
    
    [self fillOutCellViewModels];
    self.tableData = [[NSMutableArray alloc] initWithObjects:self.firstSectionArray, self.secondSectionArray, nil];
}

- (void)fillOutCellViewModels {
    
    TableViewCellModel *titleCell = [[TableViewCellModel alloc] initWithImageName:@"titleImage" placeHolder:@"Title" cellText:nil identifier:@"CustomEventEditViewControllerTextFieldCell" height:@55];
    titleCell.dropDown = NO;
    titleCell.kind = @"titleCell";
    
    TableViewCellModel *locationCell = [[TableViewCellModel alloc] initWithImageName:@"locationImage" placeHolder:@"Location" cellText:nil identifier:@"CustomEventEditViewControllerTextFieldCell" height:@55];
    locationCell.dropDown = NO;
    locationCell.kind = @"locationCell";
    
    self.firstSectionArray = @[titleCell, locationCell];
    
    TableViewCellModel *allDayCell = [[TableViewCellModel alloc] initWithImageName:@"allDayImage" placeHolder:@"All Day" cellText:nil identifier:@"CustomEventEditViewControllerSwitchCell" height:@55];
    allDayCell.dropDown = NO;
    
    self.stratDropDownCell = [[TableViewCellModel alloc] initWithImageName:nil placeHolder:nil cellText:nil identifier:@"DataPickerTableViewCell" height:@218];
    self.stratDropDownCell.dropDown = NO;
    self.stratDropDownCell.kind = @"StratDropDownCell";
    
    TableViewCellModel *startsCell = [[TableViewCellModel alloc] initWithImageName:@"startsImage" placeHolder:@"Starts" cellText:nil identifier:@"CustomEventEditViewControllerCell" height:@55];
    startsCell.dropDown = YES;
    startsCell.date = self.event.startDate;
    startsCell.dropCell = @[self.stratDropDownCell];
    startsCell.kind = @"startsCell";
    
    self.endsDropDownCell = [[TableViewCellModel alloc] initWithImageName:nil placeHolder:nil cellText:nil identifier:@"DataPickerTableViewCell" height:@218];
    self.endsDropDownCell.dropDown = NO;
    self.endsDropDownCell.kind = @"EndsDropDownCell";
    
    TableViewCellModel *endsCell = [[TableViewCellModel alloc] initWithImageName:@"endsImage" placeHolder:@"Ends" cellText:nil identifier:@"CustomEventEditViewControllerCell" height:@55];
    endsCell.dropDown = YES;
    endsCell.date = self.event.endDate;
    endsCell.dropCell = @[self.endsDropDownCell];
    endsCell.kind = @"endsCell";
    
    self.endRepeatCell= [[TableViewCellModel alloc] initWithImageName:@"endRepeatImage" placeHolder:@"End Repeat" cellText:nil identifier:@"CustomEventEditViewControllerCell" height:@55];
    self.endRepeatCell.dropDown = NO;
    self.endRepeatCell.cellText = @"Never";
    self.endRepeatCell.kind = @"endRepeatCell";
    
    self.repeatCell= [[TableViewCellModel alloc] initWithImageName:@"repeatImage" placeHolder:@"Repeat" cellText:nil identifier:@"CustomEventEditViewControllerCell" height:@55];
    self.repeatCell.dropDown = NO;
    self.repeatCell.dropCell = @[self.endRepeatCell];
    self.repeatCell.cellText = @"Never";
    self.repeatCell.kind = @"Repeat";
    
    self.palletDropDownCell = [[TableViewCellModel alloc] initWithImageName:nil placeHolder:nil cellText:nil identifier:@"PaletteColorsTableViewCell" height:@125];
    self.palletDropDownCell.dropDown = NO;
    
    TableViewCellModel *coloursCell= [[TableViewCellModel alloc] initWithImageName:@"colorImage" placeHolder:@"Colours" cellText:nil identifier:@"CustomEventEditViewControllerCell" height:@55];
    coloursCell.dropDown = YES;
    coloursCell.kind = @"coloursCell";
    coloursCell.dropCell = @[self.palletDropDownCell];
    
    self.secondSectionArray = @[allDayCell,startsCell,endsCell,self.repeatCell,coloursCell];
}


#pragma mark - Action

- (IBAction)cancelBtnPressed:(id)sender {
    
    if ([self.dismissDelegate respondsToSelector:@selector(dismissViewController)]) {
        [self.dismissDelegate dismissViewController];
    }
}

- (IBAction)addBtnPressed:(id)sender {
    
    if ([self.dismissDelegate respondsToSelector:@selector(dismissViewController)]) {

        if (self.testCalendar == nil) {
            
            self.testCalendar = [self.eventKitSupport.eventStore defaultCalendarForNewEvents];
            self.event.calendar = self.testCalendar;
        }
        else {
            self.event.calendar = self.testCalendar;
        }
        
        if (!self.switcherStateIsChanged) {
            
            self.event.allDay = NO;
        }
        
        [self.eventKitSupport saveEvent:self.event completion:^(BOOL completion) {
            [self.dismissDelegate dismissViewController];
        }];
    }
}

- (EKEventStore*)eventStore
{
    return self.eventKitSupport.eventStore;
}

- (void)dismissViewController {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (self.event.recurrenceRules.count > 0) {
        
        if (self.repeatCell.isOpen) {
            
        }
        else {
            
            self.repeatCell.isOpen = YES;
            NSMutableArray *cellDataArray = [[NSMutableArray alloc] initWithArray:[self.tableData objectAtIndex:1]];
            NSUInteger index = [cellDataArray indexOfObject:self.repeatCell];
            [cellDataArray insertObject:[self.repeatCell.dropCell firstObject] atIndex:index + 1];
            [self.tableData replaceObjectAtIndex:1 withObject:cellDataArray];
        }
    }
    else {
        
        if (self.repeatCell.isOpen) {
            self.repeatCell.isOpen = NO;
            NSMutableArray *cellDataArray = [[NSMutableArray alloc] initWithArray:[self.tableData objectAtIndex:1]];
            NSUInteger index = [cellDataArray indexOfObject:self.repeatCell];
            [cellDataArray removeObjectAtIndex:index +1];
            [self.tableData replaceObjectAtIndex:1 withObject:cellDataArray];
        }
        else {
            
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.tableData.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 32;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *cellDataArray = [self.tableData objectAtIndex:section];
    return cellDataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *cellDataArray = [self.tableData objectAtIndex:indexPath.section];
    TableViewCellModel *dict = [cellDataArray objectAtIndex:indexPath.row];
    
    return [dict.height floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSArray *cellDataArray = [self.tableData objectAtIndex:indexPath.section];
    
    TableViewCellModel *tempModel = [cellDataArray objectAtIndex:indexPath.row];
    
    if ([tempModel.identifier isEqualToString:@"CustomEventEditViewControllerTextFieldCell"]) {
        
        CustomEventEditViewControllerTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:tempModel.identifier];
        
        [cell setCellData:tempModel];
        
        cell.actionTextViewBlock = ^(NSString *title){
            
            self.event.title = title;
            tempModel.cellText = title;
        };
        
        cell.actionBtnBlock = ^(void){
            
            if ([tempModel.kind isEqualToString:@"locationCell"]) {
                
                self.event.structuredLocation = nil;
            }
            tempModel.cellText = @"";
        };
        
        return cell;
    }
    else if ([tempModel.identifier isEqualToString:@"CustomEventEditViewControllerSwitchCell"]) {
        
        CustomEventEditViewControllerSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:tempModel.identifier];
        
        cell.actionDelegate = self;
        [cell setCellData:tempModel];
        
        return cell;
    }
    else if ([tempModel.identifier isEqualToString:@"CustomEventEditViewControllerCell"]) {
        
        CustomEventEditViewControllerCell *cell = [tableView dequeueReusableCellWithIdentifier:tempModel.identifier];
        
        [cell setCellData:tempModel];
        
        return cell;
    }
    else if ([tempModel.identifier isEqualToString:@"DataPickerTableViewCell"]) {
        
        DataPickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tempModel.identifier];
        cell.actionDelegate = self;
        [cell setCellData:tempModel];
        
        return cell;
    }
    else if ([tempModel.identifier isEqualToString:@"PaletteColorsTableViewCell"]) {
        
        PaletteColorsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tempModel.identifier];
        cell.actionDelegate = self;
        [cell setCellData];
        return cell;
    }

    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray *cellDataArray = [[NSMutableArray alloc] initWithArray:[self.tableData objectAtIndex:indexPath.section]];
    TableViewCellModel *tempModel = [cellDataArray objectAtIndex:indexPath.row];
    
    if ([tempModel.kind isEqualToString:@"Repeat"]) {
        
        CustomRepeatViewController *vc = [[CustomRepeatViewController alloc] initCustomRepeatViewControllerWithEvent:self.event];
        vc.actionDelegate = self;
        vc.repeatCell = tempModel;
        
        [self showDetailViewController:vc sender:self];
    }
    if ([tempModel.kind isEqualToString:@"endRepeatCell"]) {
        
        EndRepeatViewController *vc = [[EndRepeatViewController alloc] initEndRepeatViewControllerWithEvent:self.event];
        vc.actionDelegate = self;
        vc.endRepeatCell = tempModel;
        vc.repeatCell = self.repeatCell;
        
        [self showDetailViewController:vc sender:self];
    }
    if ([tempModel.kind isEqualToString:@"locationCell"]) {
        
        LocationSearchController *vc = [[LocationSearchController alloc] initLocationSearchControllerWithModel:tempModel event:self.event];
        vc.actionDelegate = self;
        [self showDetailViewController:vc sender:self];
    }
    
    [self setDropDownforIndexPath:indexPath];

    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)setDropDownforIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray *cellDataArray = [[NSMutableArray alloc] initWithArray:[self.tableData objectAtIndex:indexPath.section]];
    TableViewCellModel *tempModel = [cellDataArray objectAtIndex:indexPath.row];
    
    if (tempModel.dropDown == YES) {
        
        if (tempModel.isOpen) {
            tempModel.isOpen = NO;
            [cellDataArray removeObjectAtIndex:indexPath.row +1];
        }
        else  {
            
            tempModel.isOpen = YES;
            [cellDataArray insertObject:[tempModel.dropCell firstObject] atIndex:indexPath.row + 1];
        }
        
        [self.tableData replaceObjectAtIndex:indexPath.section withObject:cellDataArray];
    }
}


#pragma mark - DropDownActionDelegate

- (void)changePickerDate:(NSDate *)date forKind:(NSString *)kind{
    
    if ([kind isEqualToString:@"StratDropDownCell"]) {
        
        self.stratDropDownCell.date = date;
        self.event.startDate = date;
    }
    else {
        
        self.endsDropDownCell.date = date;
        self.event.endDate = date;
    }
    
    [self.tableView reloadData];
}

- (void)changePalletColour:(EKCalendar *)calendar {
    
    self.testCalendar = calendar;
}

- (void)changeSwitchStateIsOn:(BOOL)isOn {
    
    self.switcherStateIsChanged = YES;
    self.event.allDay = isOn;
}

@end
