//
//  AppSetting.swift
//  calendar
//
//  Created by forever on 8/22/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

import UIKit

class AppSetting: NSObject {

    static let BG_COLOR = 0x27a28e
    
    static func colorize (hex: Int, alpha: Double = 1.0) -> UIColor {
        let red = Double((hex & 0xFF0000) >> 16) / 255.0
        let green = Double((hex & 0xFF00) >> 8) / 255.0
        let blue = Double((hex & 0xFF)) / 255.0
        let color: UIColor = UIColor( red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha:CGFloat(alpha) )
        return color
    }
    
}
