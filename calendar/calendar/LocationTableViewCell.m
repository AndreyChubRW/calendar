//
//  LocationTableViewCell.m
//  calendar
//
//  Created by Andrey on 9/21/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "LocationTableViewCell.h"

@interface LocationTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLabel;

@end

@implementation LocationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCellData:(MKMapItem *)mkItem {
    
    self.nameLabel.text = mkItem.name;
    MKPlacemark *placeMark = [[MKPlacemark alloc] initWithPlacemark:mkItem.placemark];
    NSArray *lines = [placeMark.addressDictionary valueForKey:@"FormattedAddressLines"];
    
    NSString *appStr = @"";
    
    for (NSString *string in lines) {
        appStr = [appStr stringByAppendingString:[NSString stringWithFormat:@" %@", string]];
    }
    
    self.placeHolderLabel.text = appStr;
    
}

@end
