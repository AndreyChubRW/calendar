//
//  SettingCalendarTableViewCell.swift
//  calendar
//
//  Created by forever on 8/20/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

import UIKit

class SettingCalendarTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onSwitchButton(sender: AnyObject) {
    }
}
