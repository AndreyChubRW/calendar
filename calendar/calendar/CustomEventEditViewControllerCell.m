//
//  CustomEventEditViewControllerCell.m
//  calendar
//
//  Created by Andrey on 9/16/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import "CustomEventEditViewControllerCell.h"

@interface CustomEventEditViewControllerCell ()

@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *detailArrowImageView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation CustomEventEditViewControllerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCellData:(TableViewCellModel *)dict {
    
    self.infoLabel.text = @"";
    
    NSDateFormatter *dateFomatter = [[NSDateFormatter alloc] init];
    [dateFomatter setDateFormat:@"E, MMM d, HH:mm"];
    
    if ([dict.kind isEqualToString:@"Repeat"]) {
        
        self.infoLabel.text = dict.cellText;
    }
    
    if ([dict.kind isEqualToString:@"startsCell"]) {
        
        TableViewCellModel *tempModel = [dict.dropCell firstObject];
        if (tempModel.date == nil) {
            self.infoLabel.text = [dateFomatter stringFromDate:dict.date];
        } else {
            self.infoLabel.text = [dateFomatter stringFromDate:tempModel.date];
        }
    }
    if ([dict.kind isEqualToString:@"endsCell"]) {
        
        TableViewCellModel *tempModel = [dict.dropCell firstObject];
        if (tempModel.date == nil) {
            self.infoLabel.text = [dateFomatter stringFromDate:dict.date];
        } else {
            self.infoLabel.text = [dateFomatter stringFromDate:tempModel.date];
        }
    }
    if ([dict.kind isEqualToString:@"endRepeatCell"]) {
        self.infoLabel.text = dict.cellText;
    }
    if ([dict.kind isEqualToString:@"coloursCell"]) {
    }
    
    self.pictureImageView.image = [UIImage imageNamed:dict.pictureName];
    self.titleLabel.text = dict.placeHolder;
    
}

- (void)setEventDate:(NSDate *)eventDate {
    
    NSDateFormatter *dateFomatter = [[NSDateFormatter alloc] init];
    [dateFomatter setDateFormat:@"E, MMM d, HH:mm"];
    self.infoLabel.text = [dateFomatter stringFromDate:eventDate];
}

- (void)setRecurrenceTitle:(NSString *)recurrenceTitle {
    
    self.infoLabel.text = recurrenceTitle;
}

@end
