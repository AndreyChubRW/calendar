//
//  SettingDateTableViewCell.swift
//  calendar
//
//  Created by forever on 8/20/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

import UIKit

class SettingDateTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDataAndTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
