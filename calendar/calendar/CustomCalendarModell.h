//
//  CustomCalendarModell.h
//  calendar
//
//  Created by Andrey on 9/22/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKitUI/EventKitUI.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUIDefines.h>
#import "MGCEventKitSupport.h"
#import <UIKit/UIKit.h>

@interface CustomCalendarModell : NSObject

@property (strong, nonatomic) EKCalendar *calendar;
@property (strong, nonatomic) UIColor *colour;
@property (strong, nonatomic) NSString *colourTitle;

@end
