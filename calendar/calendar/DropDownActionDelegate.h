//
//  DropDownActionDelegate.h
//  calendar
//
//  Created by Andrey on 9/17/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKitUI/EventKitUI.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUIDefines.h>

@protocol DropDownActionDelegate <NSObject>

@optional
- (void)changePickerDate:(NSDate *)date forKind:(NSString *)kind;
- (void)changePalletColour:(EKCalendar *)calendar;
- (void)changeSwitchStateIsOn:(BOOL)isOn;

@end
