//
//  EKEventEditTableViewCell.h
//  calendar
//
//  Created by Andrey Chub on 9/15/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface EKEventEditTableViewCell : UITableViewCell

@property (strong, nonatomic) NSDictionary *cellDict;

@property (assign, nonatomic) BOOL isFirstSection;
@property (assign, nonatomic) BOOL isThisFirstElementOfTheSecondSection;
@property (assign, nonatomic) BOOL isPickerCell;
@property (assign, nonatomic) BOOL isCellWithPushArrow;


@end
