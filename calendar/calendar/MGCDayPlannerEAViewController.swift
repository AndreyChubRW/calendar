//
//  MGCDayPlannerEAViewController.swift
//  TestCalendarLib
//
//  Created by eanton on 28/06/16.
//  Copyright © 2016 eanton. All rights reserved.
//

import Foundation
import UIKit
//import CalendarLib


@objc protocol MGCDayPlannerEAViewControllerDelegate {
    optional func dayPlannerEAEViewController(vc: MGCDayPlannerEAViewController, willPresentEventViewController eventViewController: EditEventViewController)
    
    optional func dayPlannerEAViewController(vc: MGCDayPlannerEAViewController, navigationControllerForPresentingEventViewController eventViewController: EditEventViewController) -> UINavigationController
}

enum EventType : Int {
    case TimedEventType = 1
    case AllDayEventType = 2
}


class EAEvent
{
    var startDate:NSDate?
    var endDate:NSDate?
    var title:String?
    var type:EventType?
    var location:String?
}

class MGCDayPlannerEAViewController : MGCDayPlannerViewController, UIPopoverPresentationControllerDelegate
{
    var calendar: NSCalendar?
        {
        didSet {
           self.dayPlannerView.calendar = self.calendar
        }
    }
    
    var delegate: MGCDayPlannerEAViewControllerDelegate?
    
    var  bgQueue: dispatch_queue_t?
    var daysToLoad:[NSDate] = []
    var eventsCache: [NSDate: [AnyObject]]?
    var createdEventType:Int = 0
    var createdEventDate: NSDate?
    
    var data : [EAEvent]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventsCache = [NSDate:[AnyObject]]()//NSCache()
       // self.eventsCache.countLimit = cacheSize
       
        self.bgQueue = dispatch_queue_create("MGCDayPlannerEAViewController.bgQueue", nil)
        self.reloadEvents()
        
        
        self.dayPlannerView.registerClass(MGCStandardEventView.self, forEventViewWithReuseIdentifier: "EventCellReuseIdentifier")
    }
    
    func reloadEvents() {
        for date in self.daysToLoad {
            self.dayPlannerView.setActivityIndicatorVisible(false, forDate: date)
        }
        
        self.daysToLoad.removeAll()
        self.eventsCache!.removeAll()
        self.fetchEventsInDateRange(self.dayPlannerView.visibleDays)
        self.dayPlannerView.reloadAllEvents()
    }
    
    func fetchEventsInDateRange(range:MGCDateRange)
    {
        range.start = self.calendar?.startOfDayForDate(range.start)// self.calendar!.mgc_startOfDayForDate(range.start)
        range.end = self.calendar!.startOfDayForDate(range.end)//.mgc_nextStartOfDayForDate(range.end)
        range.enumerateDaysWithCalendar(self.calendar, usingBlock: {(date: NSDate!, stop: UnsafeMutablePointer<ObjCBool>) in
            let dayEnd: NSDate = self.calendar!.mgc_nextStartOfDayForDate(date)
            let events: [AnyObject] = self.fetchEventsFrom(date, endDate: dayEnd, calendars: nil)
            self.eventsCache?[date] = events
            })
    }
    
    func fetchEventsFrom(startDate: NSDate, endDate: NSDate, calendars: [AnyObject]?) -> [AnyObject] {
        var filtered = [AnyObject]()
        for dato in data!
        {
            if ((dato as EAEvent).startDate!.compare(startDate) == NSComparisonResult.OrderedDescending || (dato as EAEvent).startDate!.compare(startDate) == NSComparisonResult.OrderedSame) && ((dato as EAEvent).endDate!.compare(endDate) == NSComparisonResult.OrderedAscending || (dato as EAEvent).endDate!.compare(endDate) == NSComparisonResult.OrderedSame)
            {
                filtered.append(dato)
            }
        }
        return filtered
    }
    
    func eventsForDay(date: NSDate) -> [AnyObject] {
        let dayStart: NSDate = self.calendar!.startOfDayForDate(date)        
        var eventsTemp = [AnyObject]()
        //let temp = self.eventsCache?.objectForKey(dayStart)  as? [AnyObject]
        let temp = self.eventsCache?[dayStart]
        
        
        if temp == nil || temp?.count == 0
        {
            let dayEnd: NSDate = self.calendar!.mgc_nextStartOfDayForDate(dayStart)
            eventsTemp = self.fetchEventsFrom(dayStart, endDate: dayEnd, calendars: nil)
            self.eventsCache?[dayStart] = eventsTemp
        }
        else
        {
            eventsTemp = temp!
        }
        
        return eventsTemp
    }
    
    func eventsOfType(type: EventType?, forDay date: NSDate) -> [AnyObject] {
        let events: [AnyObject] = self.eventsForDay(date)
//        let filteredEvents: [AnyObject] = NSMutableArray() as [AnyObject]
       
        return events
        
    }
    
    func eventOfType(type: MGCEventType, atIndex index: UInt, date: NSDate) -> EAEvent
    {
        var events: [AnyObject]? = nil
        if type == MGCEventType.AllDayEventType {
            events = self.eventsOfType(EventType.AllDayEventType, forDay: date)
        }
        else if type == MGCEventType.TimedEventType {
            events = self.eventsOfType(EventType.TimedEventType, forDay: date)
        }
        
        return events![Int(index)] as! EAEvent
    }
    
    
    
        
    override func dayPlannerView(weekView: MGCDayPlannerView, numberOfEventsOfType type: MGCEventType, atDate date: NSDate) -> Int {
        var count: Int = 0
        
        count = self.eventsOfType(nil, forDay: date).count
       
        return count
    }
    
    override func dayPlannerView(view: MGCDayPlannerView, viewForEventOfType type: MGCEventType, atIndex index: UInt, date: NSDate) -> MGCEventView {
        let ev: EAEvent = self.eventOfType(type, atIndex: index, date: date)
        let evCell: MGCStandardEventView = (view.dequeueReusableViewWithIdentifier("EventCellReuseIdentifier", forEventOfType: type, atIndex: index, date: date) as! MGCStandardEventView)
        evCell.font = UIFont.systemFontOfSize(8)
        evCell.title = ev.title
        evCell.subtitle = ev.location
        evCell.color = UIColor.orangeColor()//UIColor(CGColor: ev.calendar.CGColor)
        evCell.style = [MGCStandardEventViewStyle.Plain,MGCStandardEventViewStyle.Subtitle]
        return evCell
    }
    
   override func dayPlannerView(view: MGCDayPlannerView, dateRangeForEventOfType type: MGCEventType, atIndex index: UInt, date: NSDate) -> MGCDateRange {
        let ev: EAEvent = self.eventOfType(type, atIndex: index, date: date)
        var end: NSDate = ev.endDate!
        if ev.type == EventType.AllDayEventType {
            end = self.calendar!.mgc_nextStartOfDayForDate(end)
        }
        return MGCDateRange(start: ev.startDate, end: end)
    }
   
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        self.dayPlannerView.deselectEvent()
    }
    
    
    //MGCDayPlannerViewDelegate
    
    override func dayPlannerView(view: MGCDayPlannerView!, didSelectEventOfType type: MGCEventType, atIndex index: UInt, date: NSDate!) {
        self.showEditControllerForEventOfType(type, atIndex: index, date: date)
    }
    
    func showEditControllerForEventOfType(type:MGCEventType, atIndex index:UInt, date: NSDate)
    {
        let ev: EAEvent = self.eventOfType(type, atIndex: index, date: date)
        let view: MGCEventView = self.dayPlannerView.eventViewOfType(type, atIndex: index, date: date)
        
        let eventController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("EditEventViewController") as! EditEventViewController
        eventController.event = ev
        var nc: UINavigationController? = nil
               
        nc = UINavigationController(rootViewController: eventController)
        nc!.modalPresentationStyle = UIModalPresentationStyle.Popover
        let popover = nc!.popoverPresentationController
        let visibleRect: CGRect = CGRectIntersection(self.dayPlannerView.bounds, self.dayPlannerView.convertRect(view.bounds, fromView: view))
        popover!.permittedArrowDirections = [.Left, .Right]
        popover!.delegate = self
        popover!.sourceView = self.view
        popover!.sourceRect = visibleRect
        
        self.presentViewController(nc!, animated: true, completion: nil)
    }
}
