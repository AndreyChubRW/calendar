//
//  CustomEventEditViewController.h
//  calendar
//
//  Created by Andrey on 9/16/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKitUI/EventKitUI.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUIDefines.h>
#import "MGCEventKitSupport.h"

@protocol ActionProtocol <NSObject>

- (void)dismissViewController;

@end

@interface CustomEventEditViewController : UIViewController

@property (weak, nonatomic) id <ActionProtocol> dismissDelegate;
@property (weak, nonatomic) id <EKEventEditViewDelegate> eventEditDelegate;

@property (nonatomic) NSCalendar *calendar;
@property (strong, nonatomic) EKEvent *event;
@property (strong, nonatomic) MGCEventKitSupport *eventKitSupport;

- (instancetype)initCustomEventEditViewControllerWithEvent:(EKEvent *)event eventKitSupport:(MGCEventKitSupport *)eventKitSupport;

@end
