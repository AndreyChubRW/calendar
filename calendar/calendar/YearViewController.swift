//
//  YearViewController.swift
//  calendar
//
//  Created by forever on 8/23/16.
//  Copyright © 2016 Topmobiledev. All rights reserved.
//

import UIKit
//import CalendarLib

protocol YearViewControllerDelegate: CalendarViewControllerDelegate
{
    func yearViewController(controller:YearViewController, didSelectMonthAtDate date: NSDate)
}

class YearViewController: UIViewController, CalendarViewControllerNavigation, MGCYearCalendarViewDelegate {
    var yearCalendarView:MGCYearCalendarView?
        {
        get
        {
            return self.view as? MGCYearCalendarView
        }
    }
    var calendar:NSCalendar?
    var delegate:YearViewControllerDelegate?
    var lastSelectedDate:NSDate?
    
    var centerDate :NSDate?
        {
        get{
            let visibleRange = self.yearCalendarView?.visibleMonthsRange
            if visibleRange != nil
            {
                let monthCount = self.calendar!.components(.Month, fromDate:visibleRange!.start, toDate:visibleRange!.end, options: []).month
                let comp = NSDateComponents()
                comp.day = monthCount / 2;
                let centerDate = self.calendar!.dateByAddingComponents(comp, toDate:visibleRange!.start, options:[])
                
                let startOfWeekDateComponents = calendar!.components([.YearForWeekOfYear, .WeekOfYear ], fromDate: centerDate!)
                let startOfWeek = self.calendar!.dateFromComponents(startOfWeekDateComponents)
                return startOfWeek
            }
            return nil
        }
    }
    
    var dateFormatter:NSDateFormatter?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lastSelectedDate = nil
        
        self.yearCalendarView!.delegate = self;
        self.yearCalendarView!.calendar = self.calendar
        
        dateFormatter = NSDateFormatter()
        dateFormatter!.calendar = self.calendar
        dateFormatter!.dateFormat = "yyyy"
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(YearViewController.didChangeOrientation), name: UIApplicationDidChangeStatusBarOrientationNotification, object: nil)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView() {
        let view: MGCYearCalendarView = MGCYearCalendarView(frame: CGRectNull)
        view.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        self.view = view
    }
    
    func didChangeOrientation() {
        
        let orientation = UIApplication.sharedApplication().statusBarOrientation
        
        if UIInterfaceOrientationIsLandscape(orientation) {
            self.yearCalendarView!.reloadScrollPositionOldOrientation(UIDeviceOrientation.Portrait)
            print("landscape")
        }
        else {
            self.yearCalendarView!.reloadScrollPositionOldOrientation(UIDeviceOrientation.LandscapeLeft)
            print("portrait")
        }
        
        self.yearCalendarView!.setNeedsLayout()
    }
    
    func calendarYearViewDidScroll(view: MGCYearCalendarView!) {
        let date = self.yearCalendarView!.currentDate
        if date != nil {
            self.delegate!.calendarViewController(self, didShowDate: date)
        }
        /*
        if lastSelectedDate == nil
        {
            let date = self.yearCalendarView?.dateForMonthAtPoint(self.yearCalendarView!.center)
            if date != nil{
                self.delegate?.calendarViewController(self, didShowDate: date!)
            }
            self.SetNavigationTitle(date!)
        }
        else
        {
            self.delegate?.calendarViewController(self, didShowDate: lastSelectedDate!)
            self.SetNavigationTitle(lastSelectedDate!)
        }*/
    }
    /*
    func setCalendar(calendar: NSCalendar) {
        self.calendar = calendar
        self.yearCalendarView!.calendar = calendar
        self.dateFormatter!.calendar = calendar
    }*/
    
    func calendarYearView(view: MGCYearCalendarView!, didSelectMonthAtDate date: NSDate!) {
        self.delegate?.yearViewController(self, didSelectMonthAtDate: date)
    }
    
    func moveToDate(date: NSDate, animated: Bool) {
        lastSelectedDate = date
        self.yearCalendarView?.scrollToDate(date, animated: animated)
    }
    
    func moveToNextPageAnimated(animated: Bool) {
        let comps = NSDateComponents()
        comps.year = 1
        let date = self.calendar?.dateByAddingComponents(comps, toDate: self.yearCalendarView!.visibleMonthsRange.start, options: [])
        self.moveToDate(date!, animated: animated)
    }
    
    func moveToPreviousPageAnimated(animated: Bool) {
        let comps = NSDateComponents()
        comps.year = -1
        let date = self.calendar?.dateByAddingComponents(comps, toDate: self.yearCalendarView!.visibleMonthsRange.start, options: [])
        self.moveToDate(date!, animated: animated)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
