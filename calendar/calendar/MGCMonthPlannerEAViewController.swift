//
//  MGCMonthPlannerEAViewController.swift
//  TestCalendarLib
//
//  Created by eanton on 28/06/16.
//  Copyright © 2016 eanton. All rights reserved.
//

import UIKit
//import CalendarLib

class MGCMonthPlannerEAViewController : MGCMonthPlannerViewController, UIPopoverPresentationControllerDelegate
{
    var calendar: NSCalendar?
        {
        didSet {
            self.dateFormatter.calendar = self.calendar
            self.monthPlannerView.calendar = self.calendar
        }
    }
    
    var  bgQueue: dispatch_queue_t?
    var datesForMonthsToLoad:[NSDate] = []
    var cachedMonths: [NSDate: AnyObject]?
    var dateFormatter: NSDateFormatter = NSDateFormatter()
    var visibleMonths: MGCDateRange?
    
    
    var data : [EAEvent]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cachedMonths = [NSDate: [EAEvent]]()
        self.bgQueue = dispatch_queue_create("MGCMonthPlannerEAViewController.bgQueue", nil);
        
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter.dateStyle = NSDateFormatterStyle.NoStyle
        self.dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        self.dateFormatter.calendar = self.calendar
        
        self.monthPlannerView.calendar = self.calendar
        
        self.reloadEvents()
        
        self.monthPlannerView.registerClass(MGCStandardEventView.self, forEventCellReuseIdentifier: "EventCellReuseIdentifier")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.loadEventsIfNeeded()
    }
    
    func reloadEvents() {
        self.cachedMonths?.removeAll()
        self.loadEventsIfNeeded()
    }
    
    
    func eventsAtDate(date:NSDate) -> [EAEvent]?
    {
        let firstOfMonth : NSDate = self.calendar!.mgc_startOfMonthForDate(date)
        let days = self.cachedMonths![firstOfMonth]
        
        if days != nil{
            let events = days![date]
            return events as? [EAEvent]
        }
        
        return nil
        
    }
    
    func eventAtIndex(index:UInt, date:NSDate) -> EAEvent?
    {
        let events = self.eventsAtDate(date)
        return events == nil ? nil : events![Int(index)]
    }
    
    func visibleMonthsRange() -> MGCDateRange? {
        var visibleMonthsRange: MGCDateRange? = nil
        let visibleDaysRange: MGCDateRange? = self.monthPlannerView.visibleDays
        if visibleDaysRange != nil {
            let start: NSDate = self.calendar!.mgc_startOfMonthForDate(visibleDaysRange!.start)
            let end: NSDate = self.calendar!.mgc_nextStartOfMonthForDate(visibleDaysRange!.end)
            visibleMonthsRange = MGCDateRange(start: start, end: end)
        }
        return visibleMonthsRange
    }
    
    func fetchEventsFrom(startDate: NSDate, endDate: NSDate, calendars: [AnyObject]?) -> [EAEvent] {
        var filtered = [EAEvent]()
        for dato in data!
        {
            if ((dato as EAEvent).startDate!.compare(startDate) == NSComparisonResult.OrderedDescending || (dato as EAEvent).startDate!.compare(startDate) == NSComparisonResult.OrderedSame) && ((dato as EAEvent).endDate!.compare(endDate) == NSComparisonResult.OrderedAscending || (dato as EAEvent).endDate!.compare(endDate) == NSComparisonResult.OrderedSame)
            {
                filtered.append(dato)
            }
        }
        
        let results : [EAEvent] = filtered.sort({ $0.startDate!.compare($1.startDate!) == .OrderedAscending })
        return results
    }
    
    func allEventsInDateRange(range: MGCDateRange) -> [NSDate : [EAEvent]] {
        let allEvents: [EAEvent] = self.fetchEventsFrom(range.start, endDate: range.end, calendars: nil)
        let numDaysInRange: Int = range.components(.Day, forCalendar: self.calendar).day
        
        var eventsPerDay: [NSDate : [EAEvent]] = [NSDate : [EAEvent]](minimumCapacity: numDaysInRange)
        
        for ev: EAEvent in allEvents {
            let start: NSDate = self.calendar!.mgc_startOfDayForDate(ev.startDate)
            let eventRange: MGCDateRange = MGCDateRange(start: start, end: ev.endDate)
            eventRange.intersectDateRange(range)
            eventRange.enumerateDaysWithCalendar(self.calendar, usingBlock: {(date: NSDate!, stop: UnsafeMutablePointer<ObjCBool>) in
                if eventsPerDay[date] != nil
                {
                    eventsPerDay[date]!.append(ev)
                }
                else
                {
                    eventsPerDay[date] = [ev]
                }
                
            })
        }

        
        return eventsPerDay
    }
    
    
    func addMonthToLoadingQueue(monthStart: NSDate)
    {
        self.datesForMonthsToLoad.append(monthStart)
        let end = self.calendar!.mgc_nextStartOfMonthForDate(monthStart)
        let range = MGCDateRange(start: monthStart, end: end)
        
        let dic = self.allEventsInDateRange(range)
        self.cachedMonths![monthStart] = dic
        self.monthPlannerView.reloadEventsInRange(range)
        
    }
    
    func loadEventsIfNeeded() {
        self.datesForMonthsToLoad.removeAll()
        let visibleRange: MGCDateRange? = self.visibleMonthsRange()
        
        if visibleRange != nil{
            let months: Int = visibleRange!.components(.Month, forCalendar: self.calendar).month
            for i in 0 ..< months {
                let dc: NSDateComponents = NSDateComponents()
                dc.month = i
                let date: NSDate = self.calendar!.dateByAddingComponents(dc, toDate: visibleRange!.start, options: [])!
                if self.cachedMonths![date] == nil
                {
                    self.addMonthToLoadingQueue(date)
                }
            }
        }
    }
    
 
    
    override func monthPlannerView(view: MGCMonthPlannerView!, numberOfEventsAtDate date: NSDate!) -> Int {
        var count = 0
        if self.eventsAtDate(date) != nil
        {
            count = (self.eventsAtDate(date)?.count)!
        }
        return count
    }
    
    override func monthPlannerView(view: MGCMonthPlannerView!, dateRangeForEventAtIndex index: UInt, date: NSDate!) -> MGCDateRange! {
        let events = self.eventsAtDate(date)
        let event :EAEvent? = events![Int(index)]
        var range : MGCDateRange? = nil
        if event != nil{
            range = MGCDateRange(start: event!.startDate, end: event!.endDate)
        }
        
        return range
    }
    
    override func monthPlannerView(view: MGCMonthPlannerView!, cellForEventAtIndex index: UInt, date: NSDate!) -> MGCEventView! {
        var events: [EAEvent] = self.eventsAtDate(date)!
        let event: EAEvent? = events[Int(index)]
        var evCell: MGCStandardEventView? = nil
        if event != nil {
            evCell = (view.dequeueReusableCellWithIdentifier("EventCellReuseIdentifier", forEventAtIndex: index, date: date) as! MGCStandardEventView)
            evCell!.title = event!.title
            evCell!.subtitle = event!.location
            evCell!.detail = self.dateFormatter.stringFromDate(event!.startDate!)
            evCell!.color = UIColor.orangeColor()
           // let start: NSDate = self.calendar!.startOfDayForDate(event!.startDate!)
           // let end: NSDate = self.calendar!.mgc_nextStartOfDayForDate(event!.endDate)
           // let range: MGCDateRange = MGCDateRange(start: start, end: end)
          //  var numDays: Int = range.components(.Day, forCalendar: self.calendar).day
            evCell!.style =  [MGCStandardEventViewStyle.Default , MGCStandardEventViewStyle.Dot]
            //evCell.style |= evevnt.isAllDay ?? MGCStandardEventViewStyleDetail
        }
        return evCell!
    }
    
    override func monthPlannerViewDidScroll(view: MGCMonthPlannerView!) {
        let visibleMonthsTemp = self.visibleMonthsRange()
        
        if !visibleMonthsTemp!.isEqualToDateRange(self.visibleMonths)
        {
            self.visibleMonths = visibleMonthsTemp
            self.loadEventsIfNeeded()
        }
    }
    
    override func monthPlannerView(view: MGCMonthPlannerView!, didSelectEventAtIndex index: UInt, date: NSDate!) {
        let cell = view.cellForEventAtIndex(index, date: date)
        
        if cell != nil{
            let rect = view.convertRect(cell.bounds, fromView: cell)
            self.showEditControllerForEventAtIndex(index, date:date, rect:rect)
        }
    }
    
    func showEditControllerForEventAtIndex(index:UInt, date: NSDate , rect:CGRect)
    {
        let event: EAEvent = self.eventAtIndex(index, date: date)!
        
        
        let eventController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("EditEventViewController") as! EditEventViewController
        eventController.event = event
        var nc: UINavigationController? = nil
        
        
        nc = UINavigationController(rootViewController: eventController)
        nc!.modalPresentationStyle = UIModalPresentationStyle.Popover
        let popover = nc!.popoverPresentationController
        popover!.permittedArrowDirections = [.Left, .Right]
        popover!.delegate = self
        popover!.sourceView = self.monthPlannerView
        popover!.sourceRect = rect
        
        self.presentViewController(nc!, animated: true, completion: nil)
    }
    
    override func monthPlannerView(view: MGCMonthPlannerView!, didDeselectEventAtIndex index: UInt, date: NSDate!) {
        
    }
    
    override func monthPlannerView(view: MGCMonthPlannerView!, didSelectDayCellAtDate date: NSDate!) {
        
    }
    
    func popoverPresentationController(popoverPresentationController: UIPopoverPresentationController, willRepositionPopoverToRect rect: UnsafeMutablePointer<CGRect>, inView view: AutoreleasingUnsafeMutablePointer<UIView?>) {
    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        self.monthPlannerView.deselectEvent()
    }
    
    
}