//
//  LocationManager.h
//  TweatScore
//
//  Created by Andrey on 9/13/16.
//  Copyright © 2016 RubiconWear. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface LocationManager : NSObject <MKMapViewDelegate, CLLocationManagerDelegate>

+ (void)startTrackLocation;
+ (CLLocation *)getCurrentUserLocation;
+ (CLLocationCoordinate2D)getLocationFromAddressString:(NSString*) addressStr;

@end
